package by.itstep.coworking.mapper;

import by.itstep.coworking.dto.project.ProjectCreateDto;
import by.itstep.coworking.dto.project.ProjectFullDto;
import by.itstep.coworking.dto.project.ProjectPreviewDto;
import by.itstep.coworking.dto.project.ProjectUpdateDto;
import by.itstep.coworking.entity.ProjectEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = JoinRequestMapper.class)
public interface ProjectMapper {

    ProjectMapper PROJECT_MAPPER = Mappers.getMapper(ProjectMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "team", ignore = true)
    @Mapping(target = "requests", ignore = true)
    @Mapping(target ="author", ignore = true)
    ProjectEntity toEntity(ProjectCreateDto createDto);

    @Mapping(target = "team", ignore = true)
    @Mapping(target = "requests", ignore = true)
    @Mapping(target ="author", ignore = true)
    ProjectEntity toEntity(ProjectUpdateDto updateDto);

    @Mapping(target = "authorId", source = "author.id")
    @Mapping(target = "teamId", source = "team.id")
    ProjectFullDto toFullDto(ProjectEntity entity);

    @Mapping(target = "authorId", source = "author.id")
    ProjectPreviewDto toPreviewDto(ProjectEntity entity);
}

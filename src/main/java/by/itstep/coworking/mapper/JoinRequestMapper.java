package by.itstep.coworking.mapper;

import by.itstep.coworking.dto.joinRequest.JoinRequestCreateDto;
import by.itstep.coworking.dto.joinRequest.JoinRequestFullDto;
import by.itstep.coworking.entity.JoinRequestEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface JoinRequestMapper {

    JoinRequestMapper JOIN_REQUEST_MAPPER = Mappers.getMapper(JoinRequestMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "project", ignore = true)
    @Mapping(target = "member", ignore = true)
    @Mapping(target = "approved", ignore = true)
    JoinRequestEntity toEntity(JoinRequestCreateDto createDto);

    @Mapping(target = "memberId", source = "member.id")
    @Mapping(target = "projectId", source = "project.id")
    JoinRequestFullDto toFullDto(JoinRequestEntity entity);
}

package by.itstep.coworking.mapper;

import by.itstep.coworking.dto.team.TeamCreateDto;
import by.itstep.coworking.dto.team.TeamFullDto;
import by.itstep.coworking.dto.team.TeamUpdateDto;
import by.itstep.coworking.entity.TeamEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = {TeamMemberMapper.class, ProjectMapper.class})
public interface TeamMapper {

    TeamMapper TEAM_MAPPER = Mappers.getMapper(TeamMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "project", ignore = true)
    @Mapping(target = "members", ignore = true)
    TeamEntity toEntity(TeamCreateDto createDto);

    @Mapping(target = "project", ignore = true)
    @Mapping(target = "members", ignore = true)
    TeamEntity toEntity(TeamUpdateDto updateDto);

    TeamFullDto toFullDto(TeamEntity entity);
}

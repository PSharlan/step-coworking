package by.itstep.coworking.mapper;

import by.itstep.coworking.dto.teamMember.TeamMemberCreateDto;
import by.itstep.coworking.dto.teamMember.TeamMemberFullDto;
import by.itstep.coworking.entity.TeamMemberEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = {JoinRequestMapper.class, ProjectMapper.class})
public interface TeamMemberMapper {

    TeamMemberMapper TEAM_MEMBER_MAPPER = Mappers.getMapper(TeamMemberMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "joinRequests", ignore = true)
    @Mapping(target = "teams", ignore = true)
    @Mapping(target = "ownedProjects", ignore = true)
    TeamMemberEntity toEntity(TeamMemberCreateDto createDto);

    TeamMemberFullDto toFullDto(TeamMemberEntity entity);
}

package by.itstep.coworking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StepCoworkingApplication {

    public static void main(String[] args) {

        SpringApplication.run(StepCoworkingApplication.class, args);

    }

}

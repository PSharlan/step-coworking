package by.itstep.coworking.service;


import by.itstep.coworking.dto.team.TeamCreateDto;
import by.itstep.coworking.dto.team.TeamFullDto;
import by.itstep.coworking.dto.team.TeamUpdateDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TeamService {

    TeamFullDto findById(Integer id);

    Page<TeamFullDto> findAll(Pageable pageable);

    TeamFullDto create(TeamCreateDto createDto);

    TeamFullDto update(TeamUpdateDto updateDto);

    void deleteById(Integer id);

    void addTeamMemberToTeam(Integer teamId, Integer memberId);
}

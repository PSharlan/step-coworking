package by.itstep.coworking.service;

import by.itstep.coworking.dto.project.ProjectCreateDto;
import by.itstep.coworking.dto.project.ProjectFullDto;
import by.itstep.coworking.dto.project.ProjectPreviewDto;
import by.itstep.coworking.dto.project.ProjectUpdateDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProjectService {

    ProjectFullDto findById(Integer id);

    Page<ProjectPreviewDto> findAll(Pageable pageable);

    ProjectFullDto create(ProjectCreateDto createDto);

    ProjectFullDto update(ProjectUpdateDto updateDto);

    void deleteById(Integer id);
}

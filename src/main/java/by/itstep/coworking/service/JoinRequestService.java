package by.itstep.coworking.service;

import by.itstep.coworking.dto.joinRequest.JoinRequestCreateDto;
import by.itstep.coworking.dto.joinRequest.JoinRequestFullDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface JoinRequestService {

    JoinRequestFullDto findById(Integer id);

    Page<JoinRequestFullDto> findAll(Pageable pageable);

    JoinRequestFullDto create(JoinRequestCreateDto createDto);

    void deleteById(Integer id);

    void accept(Integer id);
}

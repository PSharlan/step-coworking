package by.itstep.coworking.service;


import by.itstep.coworking.dto.teamMember.TeamMemberCreateDto;
import by.itstep.coworking.dto.teamMember.TeamMemberFullDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TeamMemberService {

    TeamMemberFullDto findById(Integer id);

    TeamMemberFullDto findByUserId(Integer id);

    Page<TeamMemberFullDto> findAll(Pageable pageable);

    TeamMemberFullDto create(TeamMemberCreateDto createDto);

    void deleteById(Integer id);
}

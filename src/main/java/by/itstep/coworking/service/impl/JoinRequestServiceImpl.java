package by.itstep.coworking.service.impl;

import by.itstep.coworking.dto.joinRequest.JoinRequestCreateDto;
import by.itstep.coworking.dto.joinRequest.JoinRequestFullDto;
import by.itstep.coworking.entity.JoinRequestEntity;
import by.itstep.coworking.entity.ProjectEntity;
import by.itstep.coworking.entity.TeamEntity;
import by.itstep.coworking.entity.TeamMemberEntity;
import by.itstep.coworking.exception.EntityIsNotFoundException;
import by.itstep.coworking.repository.JoinRequestRepository;
import by.itstep.coworking.repository.ProjectRepository;
import by.itstep.coworking.repository.TeamMemberRepository;
import by.itstep.coworking.service.JoinRequestService;
import by.itstep.coworking.service.TeamService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static by.itstep.coworking.mapper.JoinRequestMapper.JOIN_REQUEST_MAPPER;

@Slf4j
@Service
public class JoinRequestServiceImpl implements JoinRequestService {

    @Autowired
    private JoinRequestRepository joinRequestRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private TeamMemberRepository memberRepository;

    @Autowired
    private TeamService teamService;

    @Override
    @Transactional(readOnly = true)
    public JoinRequestFullDto findById(Integer id) {
        JoinRequestEntity joinRequestEntity = joinRequestRepository.findById(id)
                .orElseThrow(() -> new EntityIsNotFoundException("Join Request is not found by id: "+ id));

        log.info("JoinRequestService -> found join request: {} by id {}", joinRequestEntity, id);
        return JOIN_REQUEST_MAPPER.toFullDto(joinRequestEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<JoinRequestFullDto> findAll(Pageable pageable) {
        Page<JoinRequestEntity> page = joinRequestRepository.findAll(pageable);

        log.info("JoinRequestService -> found {} join requests", page.getTotalElements());
        return page.map(JOIN_REQUEST_MAPPER::toFullDto);
    }

    @Override
    @Transactional
    public JoinRequestFullDto create(JoinRequestCreateDto createDto) {

        JoinRequestEntity entityToSave = JOIN_REQUEST_MAPPER.toEntity(createDto);
        TeamMemberEntity member = memberRepository.findByUserId(createDto.getUserId());
        if(member == null){
            TeamMemberEntity memberToSave = new TeamMemberEntity();
            memberToSave.setUserId(createDto.getUserId());
            member = memberRepository.save(memberToSave);
        }

        entityToSave.setMember(member);

        ProjectEntity project = projectRepository.findById(createDto.getProjectId())
                .orElseThrow(()->
                        new EntityIsNotFoundException("Project is not found by id: "+ createDto.getProjectId()));
        entityToSave.setProject(project);

        JoinRequestEntity savedEntity = joinRequestRepository.save(entityToSave);

        log.info("JoinRequestService -> {} successfully saved", savedEntity);
        return JOIN_REQUEST_MAPPER.toFullDto(savedEntity);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        joinRequestRepository.findById(id)
                .orElseThrow(() -> new EntityIsNotFoundException("Join Request is not found by id: "+ id));
        joinRequestRepository.deleteById(id);
        log.info("JoinRequestService -> delete join request by id {}",id);
    }

    @Override
    @Transactional
    public void accept(Integer id) {
        JoinRequestEntity joinRequestToAccepted = joinRequestRepository.findById(id)
                .orElseThrow(() -> new EntityIsNotFoundException("Join Request is not found by id: "+ id));
        Integer teamId = joinRequestToAccepted.getProject().getTeam().getId();
        Integer memberId = joinRequestToAccepted.getMember().getId();

        teamService.addTeamMemberToTeam(teamId,memberId);

        joinRequestToAccepted.setApproved(true);
        log.info("JoinRequestService -> Team member id {} add to project id {}",
                memberId, teamId);
    }
}

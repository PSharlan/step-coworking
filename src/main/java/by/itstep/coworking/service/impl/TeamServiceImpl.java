package by.itstep.coworking.service.impl;

import by.itstep.coworking.dto.team.TeamCreateDto;
import by.itstep.coworking.dto.team.TeamFullDto;
import by.itstep.coworking.dto.team.TeamUpdateDto;
import by.itstep.coworking.entity.ProjectEntity;
import by.itstep.coworking.entity.TeamEntity;
import by.itstep.coworking.entity.TeamMemberEntity;
import by.itstep.coworking.exception.EntityIsNotFoundException;
import by.itstep.coworking.repository.ProjectRepository;
import by.itstep.coworking.repository.TeamMemberRepository;
import by.itstep.coworking.repository.TeamRepository;
import by.itstep.coworking.service.TeamService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static by.itstep.coworking.mapper.TeamMapper.TEAM_MAPPER;

@Slf4j
@Service
public class TeamServiceImpl implements TeamService {

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private TeamMemberRepository memberRepository;

    @Override
    @Transactional(readOnly = true)
    public TeamFullDto findById(Integer id) {
        TeamEntity foundEntity = teamRepository.findById(id)
                .orElseThrow(() -> new EntityIsNotFoundException("Team is not found by id: " + id));

        log.info("TeamService -> found team: {} by id {}", foundEntity, id);
        return TEAM_MAPPER.toFullDto(foundEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<TeamFullDto> findAll(Pageable pageable) {
        Page<TeamEntity> page = teamRepository.findAll(pageable);

        log.info("TeamService -> found {} teams",page.getTotalElements());
        return page.map(TEAM_MAPPER::toFullDto);
    }

    @Override
    @Transactional
    public TeamFullDto create(TeamCreateDto createDto) {
        TeamEntity entityToSave = TEAM_MAPPER.toEntity(createDto);

        ProjectEntity project = projectRepository.findById(createDto.getProjectId()).
                orElseThrow(() -> new EntityIsNotFoundException
                        ("Project is not found by id: " + createDto.getProjectId()));

        entityToSave.setProject(project);

        TeamEntity savedEntity = teamRepository.save(entityToSave);

        log.info("TeamService -> {} successfully saved", savedEntity);
        return TEAM_MAPPER.toFullDto(savedEntity);
    }

    @Override
    @Transactional
    public TeamFullDto update(TeamUpdateDto updateDto) {

        TeamEntity entityToUpdate = teamRepository.findById(updateDto.getId())
                .orElseThrow(() -> new EntityIsNotFoundException("Team is not found by id: " + updateDto.getId()));
        TeamEntity updatingEntity = TEAM_MAPPER.toEntity(updateDto);
        updatingEntity.setProject(entityToUpdate.getProject());
        updatingEntity.setMembers(entityToUpdate.getMembers());

        TeamEntity updatedEntity = teamRepository.save(updatingEntity);

        log.info("TeamService -> {} successfully updated", updatedEntity);
        return TEAM_MAPPER.toFullDto(updatedEntity);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        teamRepository.findById(id)
                .orElseThrow(() -> new EntityIsNotFoundException("Team is not found by id: " + id));
        teamRepository.deleteById(id);
        log.info("TeamService -> deleted team by id {}", id);
    }

    @Override
    @Transactional
    public void addTeamMemberToTeam(Integer teamId, Integer memberId) {

        TeamMemberEntity memberForAddiction = memberRepository.findById(memberId)
                .orElseThrow(() -> new EntityIsNotFoundException("Team member is not found by id: " + memberId));
        TeamEntity teamToAddiction = teamRepository.findById(teamId)
                .orElseThrow(() -> new EntityIsNotFoundException("Team is not found by id: " + teamId));
        List<TeamMemberEntity> members = teamToAddiction.getMembers();
        members.add(memberForAddiction);
        teamToAddiction.setMembers(members);
        TeamEntity savedTeam = teamRepository.save(teamToAddiction);

        log.info("TeamService -> add member: {} to team: {}", memberForAddiction, savedTeam);
    }
}

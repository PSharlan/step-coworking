package by.itstep.coworking.service.impl;

import by.itstep.coworking.dto.teamMember.TeamMemberCreateDto;
import by.itstep.coworking.dto.teamMember.TeamMemberFullDto;
import by.itstep.coworking.entity.TeamMemberEntity;
import by.itstep.coworking.exception.EntityIsNotFoundException;
import by.itstep.coworking.repository.TeamMemberRepository;
import by.itstep.coworking.service.TeamMemberService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static by.itstep.coworking.mapper.TeamMemberMapper.TEAM_MEMBER_MAPPER;

@Slf4j
@Service
public class TeamMemberServiceImpl implements TeamMemberService {

    @Autowired
    private TeamMemberRepository memberRepository;

    @Override
    @Transactional(readOnly = true)
    public TeamMemberFullDto findById(Integer id) {
        TeamMemberEntity foundEntity = memberRepository.findById(id)
                .orElseThrow(() -> new EntityIsNotFoundException("Team member is not found by id: " + id));

        log.info("TeamMemberService -> found member:{} by id {}", foundEntity, id);
        return TEAM_MEMBER_MAPPER.toFullDto(foundEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public TeamMemberFullDto findByUserId(Integer id) {
        TeamMemberEntity foundEntity = memberRepository.findByUserId(id);
        if (foundEntity == null) {
            return null;
        }
        log.info("TeamMemberService -> found member:{} by User id {}", foundEntity, id);
        return TEAM_MEMBER_MAPPER.toFullDto(foundEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<TeamMemberFullDto> findAll(Pageable pageable) {
        Page<TeamMemberEntity> page = memberRepository.findAll(pageable);

        log.info("TeamMemberService -> found {} members", page.getTotalElements());
        return page.map(TEAM_MEMBER_MAPPER::toFullDto);
    }

    @Override
    @Transactional
    public TeamMemberFullDto create(TeamMemberCreateDto createDto) {
        TeamMemberEntity entityToSave = TEAM_MEMBER_MAPPER.toEntity(createDto);
        TeamMemberEntity savedEntity = memberRepository.save(entityToSave);

        log.info("TeamMemberService -> {} successfully saved", savedEntity);
        return TEAM_MEMBER_MAPPER.toFullDto(savedEntity);
    }

    @Override
    public void deleteById(Integer id) {
        memberRepository.findById(id)
                .orElseThrow(() -> new EntityIsNotFoundException("Team member is not found by id: " + id));
        memberRepository.deleteById(id);
        log.info("TeamMemberService -> deleted member by id {}", id);
    }
}

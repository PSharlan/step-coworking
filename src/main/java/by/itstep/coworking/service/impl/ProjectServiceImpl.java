package by.itstep.coworking.service.impl;

import by.itstep.coworking.dto.project.ProjectCreateDto;
import by.itstep.coworking.dto.project.ProjectFullDto;
import by.itstep.coworking.dto.project.ProjectPreviewDto;
import by.itstep.coworking.dto.project.ProjectUpdateDto;
import by.itstep.coworking.entity.ProjectEntity;
import by.itstep.coworking.entity.TeamEntity;
import by.itstep.coworking.entity.TeamMemberEntity;
import by.itstep.coworking.exception.EntityIsNotFoundException;
import by.itstep.coworking.repository.ProjectRepository;
import by.itstep.coworking.repository.TeamMemberRepository;
import by.itstep.coworking.repository.TeamRepository;
import by.itstep.coworking.service.ProjectService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static by.itstep.coworking.mapper.ProjectMapper.PROJECT_MAPPER;

@Slf4j
@Service
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private TeamMemberRepository memberRepository;

    @Autowired
    private TeamRepository teamRepository;

    @Override
    @Transactional(readOnly = true)
    public ProjectFullDto findById(Integer id) {
        ProjectEntity foundEntity = projectRepository.findById(id)
                .orElseThrow(() -> new EntityIsNotFoundException("Project is not found by id: "+ id));

        log.info("ProjectService -> found project {} by id {}", foundEntity, id);
        return PROJECT_MAPPER.toFullDto(foundEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ProjectPreviewDto> findAll(Pageable pageable) {
        Page<ProjectEntity> page = projectRepository.findAll(pageable);

        log.info("ProjectService -> found {} projects",page.getTotalElements());
        return page.map(PROJECT_MAPPER::toPreviewDto);
    }

    @Override
    @Transactional
    public ProjectFullDto create(ProjectCreateDto createDto) {

        TeamMemberEntity author = memberRepository.findByUserId(createDto.getUserId());
        if(author == null){
            TeamMemberEntity newTeamMember = new TeamMemberEntity();
            newTeamMember.setUserId(createDto.getUserId());
            author = memberRepository.save(newTeamMember);
        }

        ProjectEntity entityToSave = PROJECT_MAPPER.toEntity(createDto);
        entityToSave.setAuthor(author);
        entityToSave.setDateCreated(Instant.now());

        TeamEntity teamForProject = new TeamEntity();
        teamForProject.setName(entityToSave.getName());
        List<TeamMemberEntity> members = new ArrayList<>();
        members.add(author);
        teamForProject.setMembers(members);

        entityToSave.setTeam(teamForProject);
        ProjectEntity savedEntity = projectRepository.save(entityToSave);
        teamForProject.setProject(savedEntity);
        teamRepository.save(teamForProject);

        log.info("ProjectService -> {} successfully saved",savedEntity);
        return PROJECT_MAPPER.toFullDto(savedEntity);
    }

    @Override
    @Transactional
    public ProjectFullDto update(ProjectUpdateDto updateDto) {
        ProjectEntity entityToUpdate = projectRepository.findById(updateDto.getId())
                .orElseThrow(() -> new EntityIsNotFoundException("Project is not found by id: "+ updateDto.getId()));

        entityToUpdate.setName(updateDto.getName());
        entityToUpdate.setDescription(updateDto.getDescription());

        ProjectEntity updatedEntity = projectRepository.save(entityToUpdate);

        log.info("ProjectService -> {} successfully updated",updatedEntity);
        return PROJECT_MAPPER.toFullDto(updatedEntity);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        projectRepository.findById(id)
                .orElseThrow(()-> new EntityIsNotFoundException("Project is not found by id: " + id));
        projectRepository.deleteById(id);
        log.info("ProjectService -> delete project by id {}",id);
    }
}

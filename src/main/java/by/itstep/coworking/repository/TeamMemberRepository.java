package by.itstep.coworking.repository;

import by.itstep.coworking.entity.TeamMemberEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface TeamMemberRepository extends JpaRepository<TeamMemberEntity, Integer> {

    TeamMemberEntity findOneById(Integer id);

    TeamMemberEntity findByUserId(Integer id);
}

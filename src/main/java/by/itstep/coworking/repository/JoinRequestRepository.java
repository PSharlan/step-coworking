package by.itstep.coworking.repository;

import by.itstep.coworking.entity.JoinRequestEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JoinRequestRepository extends JpaRepository<JoinRequestEntity, Integer> {

    JoinRequestEntity findOneById(Integer id);
}

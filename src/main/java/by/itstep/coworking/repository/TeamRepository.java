package by.itstep.coworking.repository;

import by.itstep.coworking.entity.TeamEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeamRepository extends JpaRepository<TeamEntity, Integer> {

    TeamEntity findOneById(Integer id);
}

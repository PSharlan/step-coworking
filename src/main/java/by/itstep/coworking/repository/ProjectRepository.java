package by.itstep.coworking.repository;

import by.itstep.coworking.entity.ProjectEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface ProjectRepository extends JpaRepository<ProjectEntity, Integer> {

    ProjectEntity findOneById(Integer id);
}

package by.itstep.coworking.exception;

public class EntityIsNotFoundException extends RuntimeException {

    public EntityIsNotFoundException (String message) {
        super(message);
    }
}

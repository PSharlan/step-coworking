package by.itstep.coworking.dto.teamMember;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class TeamMemberCreateDto {

    @ApiModelProperty(name = "User Id", notes = "Can not be Null")
    @NotNull(message = "User id can not be empty")
    private Integer userId;
}

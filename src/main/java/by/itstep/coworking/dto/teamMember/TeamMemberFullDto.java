package by.itstep.coworking.dto.teamMember;
import by.itstep.coworking.dto.joinRequest.JoinRequestFullDto;
import by.itstep.coworking.dto.project.ProjectPreviewDto;
import by.itstep.coworking.dto.team.TeamFullDto;
import lombok.Data;
import java.util.List;

@Data
public class TeamMemberFullDto {

    private Integer id;
    private Integer userId;
    private List<JoinRequestFullDto> joinRequests;
    private List<ProjectPreviewDto> ownedProjects;
}

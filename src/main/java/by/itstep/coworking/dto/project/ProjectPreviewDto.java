package by.itstep.coworking.dto.project;
import lombok.Data;

import java.time.Instant;

@Data
public class ProjectPreviewDto {

    private Integer id;
    private Integer authorId;
    private String name;
    private String description;
    private Instant dateCreated;
}

package by.itstep.coworking.dto.project;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class ProjectUpdateDto {

    @ApiModelProperty(name = "Project Id", notes = "Can not be null")
    @NotNull(message = "Project ID can not be Null")
    private Integer id;

    @ApiModelProperty(name = "Project name", notes = "Can not be Null")
    @NotEmpty(message = "Project name can not be Empty")
    private String name;

    @ApiModelProperty(name = "Project description")
    private String description;
}

package by.itstep.coworking.dto.project;
import by.itstep.coworking.dto.joinRequest.JoinRequestFullDto;
import lombok.Data;
import java.time.Instant;
import java.util.List;

@Data
public class ProjectFullDto {

    private Integer id;
    private Integer authorId;
    private Integer teamId;
    private String name;
    private String description;
    private Instant dateCreated;
    private List<JoinRequestFullDto> requests;
}

package by.itstep.coworking.dto.project;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@Data
public class ProjectCreateDto {

    @ApiModelProperty(name = "User Id", notes = "Can not be Null")
    @NotNull(message = "User id can not be Null")
    private Integer userId;

    @ApiModelProperty(name = "Author Id (team member)")
    private Integer authorId;

    @ApiModelProperty(name = "Project name", notes = "Can not be Null")
    @NotEmpty(message = "Project name can not be empty")
    private String name;

    @ApiModelProperty(name = "Project description")
    private String description;
}

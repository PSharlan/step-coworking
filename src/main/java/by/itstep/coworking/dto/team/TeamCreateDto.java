package by.itstep.coworking.dto.team;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class TeamCreateDto {

    @ApiModelProperty(name = "Team name", notes = "Can not be empty")
    @NotEmpty(message = "Team name can not be empty")
    private String name;

    @ApiModelProperty(name = "Project Id",notes = "Can not be Null")
    @NotNull(message = "Project id can not be Null")
    private Integer projectId;
}

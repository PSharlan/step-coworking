package by.itstep.coworking.dto.team;
import by.itstep.coworking.dto.project.ProjectPreviewDto;
import by.itstep.coworking.dto.teamMember.TeamMemberFullDto;
import lombok.Data;
import java.util.List;

@Data
public class TeamFullDto {

    private Integer id;
    private String name;
    private ProjectPreviewDto project;
    private List<TeamMemberFullDto> members;
}

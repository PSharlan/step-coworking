package by.itstep.coworking.dto.joinRequest;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class JoinRequestCreateDto {

    @ApiModelProperty(name = "Message")
    private String message;

    @ApiModelProperty(name = "User Id", notes = "Can not be Null")
    @NotNull(message = "User id can not be null")
    private Integer userId;

    @ApiModelProperty(name = "Project Id", notes = "Can not be Null")
    @NotNull(message = "Project id can not be null")
    private Integer projectId;
}

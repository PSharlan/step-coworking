package by.itstep.coworking.dto.joinRequest;
import lombok.Data;

@Data
public class JoinRequestFullDto {

    private Integer id;
    private String message;
    private Boolean approved;
    private Integer memberId;
    private Integer projectId;
}

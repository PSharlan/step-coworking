package by.itstep.coworking.controller;

import by.itstep.coworking.dto.teamMember.TeamMemberCreateDto;
import by.itstep.coworking.dto.teamMember.TeamMemberFullDto;
import by.itstep.coworking.service.TeamMemberService;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class TeamMemberController {

    @Autowired
    TeamMemberService teamMemberService;

    @ApiModelProperty(value = "Find team member by Id", notes = "Existing Id must be specified")
    @GetMapping("/teamMembers/{id}")
    public TeamMemberFullDto getById(@PathVariable Integer id){
        return teamMemberService.findById(id);
    }

    @ApiModelProperty(value = "Find all team members")
    @GetMapping("/teamMembers")
    public Page<TeamMemberFullDto> getAll(@PageableDefault(size = Integer.MAX_VALUE)Pageable pageable){
        return teamMemberService.findAll(pageable);
    }

    @ApiModelProperty(value = "Create new team member")
    @PostMapping("/teamMembers")
    public TeamMemberFullDto create(@Valid @RequestBody TeamMemberCreateDto createDto){
        return teamMemberService.create(createDto);
    }

    @ApiModelProperty(value = "Delete team member by Id")
    @DeleteMapping("/teamMembers/{id}")
    public void deleteById (@PathVariable Integer id){
        teamMemberService.deleteById(id);
    }
}

package by.itstep.coworking.controller;

import by.itstep.coworking.dto.project.ProjectCreateDto;
import by.itstep.coworking.dto.project.ProjectFullDto;
import by.itstep.coworking.dto.project.ProjectPreviewDto;
import by.itstep.coworking.dto.project.ProjectUpdateDto;
import by.itstep.coworking.service.ProjectService;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @ApiModelProperty(value = "Find one project by Id", notes = "Existing id must be specified")
    @GetMapping("/projects/{id}")
    public ProjectFullDto getById(@PathVariable Integer id){
        return projectService.findById(id);
    }

    @ApiModelProperty(value = "Find all projects")
    @GetMapping("/projects")
    public Page<ProjectPreviewDto> findAll(@PageableDefault(size = Integer.MAX_VALUE)Pageable pageable){
        return projectService.findAll(pageable);
    }

    @ApiModelProperty(value = "Create new project")
    @PostMapping("/projects")
    public ProjectFullDto create(@Valid @RequestBody ProjectCreateDto createDto){
        return projectService.create(createDto);
    }

    @ApiModelProperty(value = "Update project")
    @PutMapping("/projects")
    public ProjectFullDto update(@Valid @RequestBody ProjectUpdateDto updateDto){
        return projectService.update(updateDto);
    }

    @ApiModelProperty(value = "Delete project by Id")
    @DeleteMapping("/projects/{id}")
    public void delete(@PathVariable Integer id){

        projectService.deleteById(id);
    }

}

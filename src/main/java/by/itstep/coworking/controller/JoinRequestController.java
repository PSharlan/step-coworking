package by.itstep.coworking.controller;

import by.itstep.coworking.dto.joinRequest.JoinRequestCreateDto;
import by.itstep.coworking.dto.joinRequest.JoinRequestFullDto;
import by.itstep.coworking.service.JoinRequestService;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
public class JoinRequestController {

    @Autowired
    private JoinRequestService joinRequestService;

    @GetMapping("/joinRequests/{id}")
    @ApiModelProperty(value = "Find one join request by Id", notes = "Existing Id must be specified")
    public JoinRequestFullDto getById(@PathVariable Integer id){
        return joinRequestService.findById(id);
    }

    @ApiModelProperty(value = "Find all join requests")
    @GetMapping("/joinRequests")
    public Page<JoinRequestFullDto> findAll(@PageableDefault(size = Integer.MAX_VALUE)Pageable pageable){
        return joinRequestService.findAll(pageable);
    }

    @ApiModelProperty(value = "Create join request")
    @PostMapping("/joinRequests")
    public JoinRequestFullDto create(@Valid @RequestBody JoinRequestCreateDto createDto){
        return joinRequestService.create(createDto);
    }

    @ApiModelProperty(value = "Accepted join request and add member to team")
    @PutMapping("/joinRequestAccept/{id}")
    public void accept(@PathVariable Integer id){
        joinRequestService.accept(id);
    }

    @ApiModelProperty(value = "Delete join request")
    @DeleteMapping("/joinRequests/{id}")
    public void delete(@PathVariable Integer id){
        joinRequestService.deleteById(id);
    }
}

package by.itstep.coworking.controller;

import by.itstep.coworking.dto.team.TeamCreateDto;
import by.itstep.coworking.dto.team.TeamFullDto;
import by.itstep.coworking.dto.team.TeamUpdateDto;
import by.itstep.coworking.service.TeamService;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class TeamController {

    @Autowired
    private TeamService teamService;

    @ApiModelProperty(value = "Find team by Id", notes = "Existing Id mist be specified")
    @GetMapping("/teams/{id}")
    public TeamFullDto getById(@PathVariable Integer id){
        return teamService.findById(id);
    }

    @ApiModelProperty(value = "Find all teams")
    @GetMapping("/teams")
    public Page<TeamFullDto> getAll(@PageableDefault(size = Integer.MAX_VALUE)Pageable pageable){
        return teamService.findAll(pageable);
    }

    @ApiModelProperty(value = "Create new team")
    @PostMapping("/teams")
    public TeamFullDto create(@Valid @RequestBody TeamCreateDto createDto){
        return teamService.create(createDto);
    }

    @ApiModelProperty(value = "Update team")
    @PutMapping("/teams")
    public TeamFullDto update(@Valid @RequestBody TeamUpdateDto updateDto){
        return teamService.update(updateDto);
    }

    @ApiModelProperty(value = "Delete team by Id")
    @DeleteMapping("/teams/{id}")
    public void deleteById(@PathVariable Integer id){
        teamService.deleteById(id);
    }

    @ApiModelProperty(value = "Add new member to team")
    @GetMapping("/teams/{teamId}/{memberId}")
    public void addTeamMemberToTeam(@PathVariable Integer teamId, @PathVariable Integer memberId){
        teamService.addTeamMemberToTeam(teamId,memberId);
    }
}

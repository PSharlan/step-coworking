package by.itstep.coworking.controller;

import by.itstep.coworking.dto.team.TeamCreateDto;
import by.itstep.coworking.dto.team.TeamFullDto;
import by.itstep.coworking.dto.team.TeamUpdateDto;
import by.itstep.coworking.entity.ProjectEntity;
import by.itstep.coworking.entity.TeamEntity;
import by.itstep.coworking.entity.TeamMemberEntity;
import by.itstep.coworking.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.List;

import static by.itstep.coworking.EntityGenerationsUtils.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class TeamControllerTest extends AbstractIntegrationTest {

    @Test
    void testGetById_HappyPath() throws Exception {
        //given
        TeamMemberEntity savedMember = memberRepository.save(generateTeamMember());
        ProjectEntity projectToSave = generateProject();
        projectToSave.setAuthor(savedMember);
        ProjectEntity savedProject = projectRepository.save(projectToSave);

        List<TeamMemberEntity> membersList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            TeamMemberEntity savedMemberEntity = memberRepository.save(generateTeamMember());
            membersList.add(savedMemberEntity);
        }

        TeamEntity teamToSave = generateTeam();
        teamToSave.setProject(savedProject);
        teamToSave.setMembers(membersList);
        TeamEntity savedTeam = teamRepository.save(teamToSave);

        //when
        MvcResult result = mockMvc.perform(get("/teams/{id}", savedTeam.getId()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        TeamFullDto foundTeam = objectMapper.readValue(bytes, TeamFullDto.class);

        //then
        Assertions.assertNotNull(foundTeam);
        Assertions.assertNotNull(foundTeam.getId());
        Assertions.assertEquals(foundTeam.getId(), savedTeam.getId());
        Assertions.assertEquals(foundTeam.getMembers().size(), savedTeam.getMembers().size());
        Assertions.assertEquals(foundTeam.getProject().getId(), savedTeam.getProject().getId());
    }

    @Test
    void testGetById_WhenNotFound() throws Exception {
        //given
        int notExitingId = 0;

        //when - //then
        mockMvc.perform(get("/teams/{id}", notExitingId))
                .andExpect(status().isNotFound());
    }

    @Test
    void testFindAll_HappyPath() throws Exception {
        //given
        List<TeamEntity> teamEntities = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            TeamMemberEntity savedMember = memberRepository.save(generateTeamMember());
            ProjectEntity projectToSave = generateProject();
            projectToSave.setAuthor(savedMember);
            ProjectEntity savedProject = projectRepository.save(projectToSave);

            List<TeamMemberEntity> membersList = new ArrayList<>();
            for (int a = 0; a < 10; a++) {
                TeamMemberEntity savedMemberEntity = memberRepository.save(generateTeamMember());
                membersList.add(savedMemberEntity);
            }

            TeamEntity teamToSave = generateTeam();
            teamToSave.setProject(savedProject);
            teamToSave.setMembers(membersList);
            TeamEntity savedTeam = teamRepository.save(teamToSave);
            teamEntities.add(savedTeam);
        }

        Pageable page = PageRequest.of(0,10);

        //when-//then
        mockMvc.perform(get("/teams",page))
                .andExpect(status().isOk());
    }

    @Test
    void testCreate_HappyPath() throws Exception {
        //given
        TeamMemberEntity savedMember = memberRepository.save(generateTeamMember());
        ProjectEntity projectToSave = generateProject();
        projectToSave.setAuthor(savedMember);
        ProjectEntity savedProject = projectRepository.save(projectToSave);

        TeamCreateDto createDto = new TeamCreateDto();
        createDto.setName("Name");
        createDto.setProjectId(savedProject.getId());

        //when
        MvcResult result = mockMvc.perform(post("/teams")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        TeamFullDto savedTeam = objectMapper.readValue(bytes, TeamFullDto.class);

        //then
        Assertions.assertNotNull(savedTeam);
    }

    @Test
    void testCreate_WhenProjectIdIsNull() throws Exception {
        //given
        TeamCreateDto createDto = new TeamCreateDto();
        createDto.setName("Name");


        //when
        mockMvc.perform(post("/teams")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());
    }


    @Test
    void testUpdate_HappyPath() throws Exception {
        //given
        TeamMemberEntity savedMember = memberRepository.save(generateTeamMember());
        ProjectEntity projectToSave = generateProject();
        projectToSave.setAuthor(savedMember);
        ProjectEntity savedProject = projectRepository.save(projectToSave);

        List<TeamMemberEntity> membersList = new ArrayList<>();
        for (int a = 0; a < 10; a++) {
            TeamMemberEntity savedMemberEntity = memberRepository.save(generateTeamMember());
            membersList.add(savedMemberEntity);
        }

        TeamEntity teamToSave = generateTeam();
        teamToSave.setProject(savedProject);
        teamToSave.setMembers(membersList);
        TeamEntity savedTeam = teamRepository.save(teamToSave);

        TeamUpdateDto updateDto = new TeamUpdateDto();
        updateDto.setId(savedTeam.getId());
        updateDto.setName("New Name");

        //when
        MvcResult result = mockMvc.perform(put("/teams")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        TeamFullDto updatedTeam = objectMapper.readValue(bytes, TeamFullDto.class);

        //then
        Assertions.assertNotNull(updatedTeam);
        Assertions.assertEquals(updatedTeam.getName(), updateDto.getName());
    }

    @Test
    void testUpdate_WhenTeamNameIsNull() throws Exception {
        //given
        TeamMemberEntity savedMember = memberRepository.save(generateTeamMember());
        ProjectEntity projectToSave = generateProject();
        projectToSave.setAuthor(savedMember);
        ProjectEntity savedProject = projectRepository.save(projectToSave);

        List<TeamMemberEntity> membersList = new ArrayList<>();
        for (int a = 0; a < 10; a++) {
            TeamMemberEntity savedMemberEntity = memberRepository.save(generateTeamMember());
            membersList.add(savedMemberEntity);
        }

        TeamEntity teamToSave = generateTeam();
        teamToSave.setProject(savedProject);
        teamToSave.setMembers(membersList);
        TeamEntity savedTeam = teamRepository.save(teamToSave);

        TeamUpdateDto updateDto = new TeamUpdateDto();
        updateDto.setId(savedTeam.getId());

        //when
        mockMvc.perform(put("/teams")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testDeleteById_HappyPath() throws Exception {
        //given
        TeamMemberEntity savedMember = memberRepository.save(generateTeamMember());
        ProjectEntity projectToSave = generateProject();
        projectToSave.setAuthor(savedMember);
        ProjectEntity savedProject = projectRepository.save(projectToSave);

        List<TeamMemberEntity> membersList = new ArrayList<>();
        for (int a = 0; a < 10; a++) {
            TeamMemberEntity savedMemberEntity = memberRepository.save(generateTeamMember());
            membersList.add(savedMemberEntity);
        }

        TeamEntity teamToSave = generateTeam();
        teamToSave.setProject(savedProject);
        teamToSave.setMembers(membersList);
        TeamEntity savedTeam = teamRepository.save(teamToSave);
        Integer idForDelete = savedTeam.getId();

        //when
        mockMvc.perform(delete("/teams/{id}", idForDelete))
                .andExpect(status().isOk());

        //then
        Assertions.assertNull(teamRepository.findOneById(idForDelete));
    }

    @Test
    void testDeleteById_WhenIdIsNotFound() throws Exception {
        // given
        int notExitingId = 0;

        //when - //then
        mockMvc.perform(delete("/teams/{id}", notExitingId))
                .andExpect(status().isNotFound());
    }

    @Test
    void testAddMemberToTeam_HappyPath() throws Exception {
        //given
        TeamMemberEntity author = memberRepository.save(generateTeamMember());
        ProjectEntity projectToSave = generateProject();
        projectToSave.setAuthor(author);
        ProjectEntity savedProject = projectRepository.save(projectToSave);

        List<TeamMemberEntity> membersList = new ArrayList<>();
        for (int a = 0; a < 10; a++) {
            TeamMemberEntity savedMemberEntity = memberRepository.save(generateTeamMember());
            membersList.add(savedMemberEntity);
        }

        TeamEntity teamToSave = generateTeam();
        teamToSave.setProject(savedProject);
        teamToSave.setMembers(membersList);
        TeamEntity savedTeam = teamRepository.save(teamToSave);
        Integer teamId = savedTeam.getId();

        TeamMemberEntity savedNewMember = memberRepository.save(generateTeamMember());
        Integer newMemberId = savedNewMember.getId();

        //when
        mockMvc.perform(get("/teams/{teamId}/{memberId}", teamId, newMemberId))
                .andExpect(status().isOk());

        //then
        TeamEntity updatedTeam = teamRepository.findOneById(teamId);
        Assertions.assertTrue(updatedTeam.getMembers().contains(savedNewMember));
    }

    @Test
    void testAddMemberToTeam_WhenTeamMemberIdIsNotFound() throws Exception {
        //given
        TeamMemberEntity author = memberRepository.save(generateTeamMember());
        ProjectEntity projectToSave = generateProject();
        projectToSave.setAuthor(author);
        ProjectEntity savedProject = projectRepository.save(projectToSave);

        List<TeamMemberEntity> membersList = new ArrayList<>();
        for (int a = 0; a < 10; a++) {
            TeamMemberEntity savedMemberEntity = memberRepository.save(generateTeamMember());
            membersList.add(savedMemberEntity);
        }

        TeamEntity teamToSave = generateTeam();
        teamToSave.setProject(savedProject);
        teamToSave.setMembers(membersList);
        TeamEntity savedTeam = teamRepository.save(teamToSave);
        Integer teamId = savedTeam.getId();

        int notExitingId = 0;

        //when - //then
        mockMvc.perform(get("/teams/{teamId}/{memberId}", teamId, notExitingId))
                .andExpect(status().isNotFound());
    }

    @Test
    void testAddMemberToTeam_WhenTeamIdIsNotFound() throws Exception {
        //given
        TeamMemberEntity author = memberRepository.save(generateTeamMember());
        ProjectEntity projectToSave = generateProject();
        projectToSave.setAuthor(author);
        ProjectEntity savedProject = projectRepository.save(projectToSave);

        List<TeamMemberEntity> membersList = new ArrayList<>();
        for (int a = 0; a < 10; a++) {
            TeamMemberEntity savedMemberEntity = memberRepository.save(generateTeamMember());
            membersList.add(savedMemberEntity);
        }

        TeamEntity teamToSave = generateTeam();
        teamToSave.setProject(savedProject);
        teamToSave.setMembers(membersList);

        int notExitingId = 0;

        TeamMemberEntity savedNewMember = memberRepository.save(generateTeamMember());
        Integer newMemberId = savedNewMember.getId();

        //when - //then
        mockMvc.perform(get("/teams/{teamId}/{memberId}", notExitingId, newMemberId))
                .andExpect(status().isNotFound());
    }
}

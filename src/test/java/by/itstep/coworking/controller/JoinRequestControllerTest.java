package by.itstep.coworking.controller;

import by.itstep.coworking.EntityGenerationsUtils;
import by.itstep.coworking.dto.joinRequest.JoinRequestCreateDto;
import by.itstep.coworking.dto.joinRequest.JoinRequestFullDto;
import by.itstep.coworking.entity.JoinRequestEntity;
import by.itstep.coworking.entity.ProjectEntity;
import by.itstep.coworking.entity.TeamEntity;
import by.itstep.coworking.entity.TeamMemberEntity;
import by.itstep.coworking.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.List;

import static by.itstep.coworking.EntityGenerationsUtils.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class JoinRequestControllerTest extends AbstractIntegrationTest {

    @Test
    void testFindAll_HappyPath() throws Exception {
        //given
        List<JoinRequestEntity> savedEntities = new ArrayList<>();

        for (int i = 0; i < 20; i++) {
            JoinRequestEntity requestEntity = generateJoinRequest();
            JoinRequestEntity savedEntity = requestRepository.save(requestEntity);

            savedEntities.add(savedEntity);
        }

        Pageable page = PageRequest.of(0,20);

        //when-//then
        mockMvc.perform(get("/joinRequests", page))
                .andExpect(status().isOk());

    }

    @Test
    void testFindById_HappyPath() throws Exception {
        //given
        JoinRequestEntity requestEntity = generateJoinRequest();
        JoinRequestEntity savedEntity = requestRepository.save(requestEntity);

        //when
        MvcResult result = mockMvc.perform(get("/joinRequests/{id}", savedEntity.getId()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        JoinRequestFullDto foundJoinRequest = objectMapper.readValue(bytes,JoinRequestFullDto.class);

        //then
        Assertions.assertNotNull(foundJoinRequest);
        Assertions.assertNotNull(foundJoinRequest.getId());
        Assertions.assertEquals(savedEntity.getId(),foundJoinRequest.getId());
    }

    @Test
    public void testFindById_WhenNotFound() throws Exception {
        //given
        int notExistingId = 0;

        //when - //then
        mockMvc.perform(get("/joinRequests/{id}", notExistingId))
                .andExpect(status().isNotFound());
    }

    @Test
    void testCreate_HappyPath() throws Exception{
        //given
        JoinRequestCreateDto createDto = new JoinRequestCreateDto();
        TeamMemberEntity savedMember = memberRepository.save(generateTeamMember());
        ProjectEntity project = EntityGenerationsUtils.generateProject();
        project.setAuthor(savedMember);
        ProjectEntity savedProject = projectRepository.save(project);


        createDto.setProjectId(savedProject.getId());
        createDto.setUserId(savedMember.getId());
        createDto.setMessage("Join request message");

        //when
        MvcResult result = mockMvc.perform(post("/joinRequests")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        JoinRequestFullDto savedJoinRequest = objectMapper.readValue(bytes,JoinRequestFullDto.class);

        //then
        Assertions.assertNotNull(savedJoinRequest);
    }

    @Test
    void testCreate_WhenUserIdIsNull() throws Exception {
        //given
        JoinRequestCreateDto createDto = new JoinRequestCreateDto();
        TeamMemberEntity savedMember = memberRepository.save(generateTeamMember());
        ProjectEntity project = EntityGenerationsUtils.generateProject();
        project.setAuthor(savedMember);
        ProjectEntity savedProject = projectRepository.save(project);


        createDto.setProjectId(savedProject.getId());
        createDto.setMessage("Join request message");

        //when
        mockMvc.perform(post("/joinRequests")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testCreate_WhenProjectIdIsNull() throws Exception {
        //given
        JoinRequestCreateDto createDto = new JoinRequestCreateDto();

        createDto.setUserId(1);
        createDto.setMessage("Join request message");

        //when
        mockMvc.perform(post("/joinRequests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testDeleteById_HappyPath() throws Exception {
        //given
        JoinRequestEntity entityToSave = generateJoinRequest();
        JoinRequestEntity savedEntity = requestRepository.save(entityToSave);
        Integer idForDelete = savedEntity.getId();

        //when
        mockMvc.perform(delete("/joinRequests/{id}",idForDelete))
                .andExpect(status().isOk());

        //then
        Assertions.assertNull(requestRepository.findOneById(idForDelete));
    }

    @Test
    void testDeleteById_WhenIdIsNotFound() throws Exception {
        //given
        int notExitingId = 0;

        //when - //then
        mockMvc.perform(delete("/joinRequests/{id}",notExitingId))
                .andExpect(status().isNotFound());
    }

    @Test
    void testAccept_HappyPath() throws Exception {
        //given
        JoinRequestEntity joinRequestEntity = new JoinRequestEntity();
        TeamMemberEntity savedMember = memberRepository.save(generateTeamMember());

        List<TeamMemberEntity> teamMemberEntities = new ArrayList<>();
        for(int i = 0; i <10; i++){
            TeamMemberEntity savedMemberForTeam = memberRepository.save(generateTeamMember());
            teamMemberEntities.add(savedMemberForTeam);
        }

        ProjectEntity project = EntityGenerationsUtils.generateProject();
        project.setAuthor(savedMember);
        ProjectEntity savedProject = projectRepository.save(project);

        TeamEntity newTeam = generateTeam();
        newTeam.setMembers(teamMemberEntities);
        newTeam.setProject(savedProject);
        teamRepository.save(newTeam);


        joinRequestEntity.setProject(savedProject);
        joinRequestEntity.setMember(savedMember);
        joinRequestEntity.setMessage("Join request message");
        JoinRequestEntity savedJoinRequest = requestRepository.save(joinRequestEntity);

        //when
        mockMvc.perform(put("/joinRequestAccept/{id}",savedJoinRequest.getId()))
                .andExpect(status().isOk())
                .andReturn();

        //then
        List<TeamMemberEntity> memberEntities = projectRepository.findOneById(savedProject.getId()).getTeam().getMembers();
        JoinRequestEntity acceptedRequest = requestRepository.findOneById(joinRequestEntity.getId());

        Assertions.assertTrue(memberEntities.contains(savedMember));
        Assertions.assertEquals(acceptedRequest.getApproved(), true);
    }

    @Test
    void testAccept_WhenJoinRequestIdIsNotFound() throws Exception {
        //given
        int notExitingId = 0;

        //when
        mockMvc.perform(put("/joinRequestAccept/{id}",notExitingId))
                .andExpect(status().isNotFound())
                .andReturn();

        //then

    }
}

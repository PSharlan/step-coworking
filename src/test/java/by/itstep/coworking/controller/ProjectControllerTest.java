package by.itstep.coworking.controller;

import by.itstep.coworking.EntityGenerationsUtils;
import by.itstep.coworking.dto.project.ProjectCreateDto;
import by.itstep.coworking.dto.project.ProjectFullDto;
import by.itstep.coworking.dto.project.ProjectUpdateDto;
import by.itstep.coworking.entity.JoinRequestEntity;
import by.itstep.coworking.entity.ProjectEntity;
import by.itstep.coworking.entity.TeamEntity;
import by.itstep.coworking.entity.TeamMemberEntity;
import by.itstep.coworking.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;



public class ProjectControllerTest extends AbstractIntegrationTest {

    @Test
    void testFindAll_HappyPath() throws Exception {
        //given
        List<ProjectEntity> projectEntities = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            ProjectEntity entityToSave = EntityGenerationsUtils.generateProject();
            TeamMemberEntity authorToSave = EntityGenerationsUtils.generateTeamMember();
            TeamMemberEntity savedAuthor = memberRepository.save(authorToSave);
            entityToSave.setAuthor(savedAuthor);
            ProjectEntity savedProject = projectRepository.save(entityToSave);
            projectEntities.add(savedProject);
        }

        Pageable page = PageRequest.of(0, 20);

        //when-//then
        mockMvc.perform(get("/projects", page))
                .andExpect(status().isOk());
    }

    @Test
    void testFindById_HappyPath() throws Exception {
        //given
        ProjectEntity entityToSave = EntityGenerationsUtils.generateProject();
        TeamMemberEntity authorToSave = EntityGenerationsUtils.generateTeamMember();
        TeamMemberEntity savedAuthor = memberRepository.save(authorToSave);
        entityToSave.setAuthor(savedAuthor);
        ProjectEntity savedProject = projectRepository.save(entityToSave);

        //when
        MvcResult result = mockMvc.perform(get("/projects/{id}", savedProject.getId()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        ProjectFullDto foundProject = objectMapper.readValue(bytes, ProjectFullDto.class);

        //then
        Assertions.assertNotNull(foundProject);
        Assertions.assertNotNull(foundProject.getId());
        Assertions.assertEquals(foundProject.getId(), savedProject.getId());
    }

    @Test
    void testFindById_WhenNotFound() throws Exception {
        //given
        int notExitingId = 0;

        //when - //then
        mockMvc.perform(get("/projects/{id}", notExitingId))
                .andExpect(status().isNotFound());
    }

    @Test
    void testCreate_HappyPath() throws Exception {
        // given
        ProjectCreateDto createDto = new ProjectCreateDto();
        TeamMemberEntity memberToSave = EntityGenerationsUtils.generateTeamMember();
        TeamMemberEntity savedMember = memberRepository.save(memberToSave);
        createDto.setAuthorId(savedMember.getId());
        createDto.setUserId(savedMember.getUserId());
        createDto.setName("Project name");
        createDto.setDescription("Project description");

        // when
        MvcResult result = mockMvc.perform(post("/projects")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        ProjectFullDto savedProject = objectMapper.readValue(bytes, ProjectFullDto.class);

        //then
        Assertions.assertNotNull(savedProject);
    }

    @Test
    void testCreate_WhenUserIdIsNull() throws Exception {
        // given
        ProjectCreateDto createDto = new ProjectCreateDto();
        createDto.setName("Project name");
        createDto.setDescription("Project description");

        //when - //then
        mockMvc.perform(post("/projects")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testCreate_WhenAuthorIdIsNull() throws Exception {
        // given
        ProjectCreateDto createDto = new ProjectCreateDto();
        TeamMemberEntity memberToSave = EntityGenerationsUtils.generateTeamMember();
        createDto.setUserId(memberToSave.getUserId());
        createDto.setName("Project name");
        createDto.setDescription("Project description");

        MvcResult result = mockMvc.perform(post("/projects")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        ProjectFullDto savedProject = objectMapper.readValue(bytes, ProjectFullDto.class);

        //then
        Assertions.assertNotNull(savedProject);
    }

    @Test
    void testUpdate_HappyPath() throws Exception {

        //given
        TeamMemberEntity authorToSave = EntityGenerationsUtils.generateTeamMember();
        TeamMemberEntity savedAuthor = memberRepository.save(authorToSave);

        List<JoinRequestEntity> requests = Arrays.asList(EntityGenerationsUtils.generateJoinRequest());

        ProjectEntity projectToSave = EntityGenerationsUtils.generateProject();
        projectToSave.setAuthor(savedAuthor);
        projectToSave.setRequests(requests);

        ProjectEntity savedProject = projectRepository.save(projectToSave);

        TeamEntity teamToSave = EntityGenerationsUtils.generateTeam();
        teamToSave.setProject(savedProject);
        teamRepository.save(teamToSave);

        ProjectUpdateDto updateDto = new ProjectUpdateDto();
        updateDto.setId(savedProject.getId());
        updateDto.setDescription("New description");
        updateDto.setName("New name");

        //when
        MvcResult result = mockMvc.perform(put("/projects")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        ProjectFullDto updatedProject = objectMapper.readValue(bytes, ProjectFullDto.class);

        //then
        Assertions.assertNotNull(updatedProject);
        Assertions.assertEquals(updatedProject.getDescription(), updateDto.getDescription());
        Assertions.assertEquals(updatedProject.getName(), updateDto.getName());
    }

    @Test
    void testUpdate_WhenNewProjectNameIsNull() throws Exception {

        //given
        TeamMemberEntity authorToSave = EntityGenerationsUtils.generateTeamMember();
        TeamMemberEntity savedAuthor = memberRepository.save(authorToSave);

        List<JoinRequestEntity> requests = Arrays.asList(EntityGenerationsUtils.generateJoinRequest());

        ProjectEntity projectToSave = EntityGenerationsUtils.generateProject();
        projectToSave.setAuthor(savedAuthor);
        projectToSave.setRequests(requests);

        ProjectEntity savedProject = projectRepository.save(projectToSave);

        TeamEntity teamToSave = EntityGenerationsUtils.generateTeam();
        teamToSave.setProject(savedProject);
        teamRepository.save(teamToSave);

        ProjectUpdateDto updateDto = new ProjectUpdateDto();
        updateDto.setId(savedProject.getId());
        updateDto.setDescription("New description");


        //when - //then
        mockMvc.perform(put("/projects")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testDeleteById_HappyPath() throws Exception {
        //given
        TeamMemberEntity authorToSave = EntityGenerationsUtils.generateTeamMember();
        TeamMemberEntity savedAuthor = memberRepository.save(authorToSave);

        ProjectEntity entityToSave = EntityGenerationsUtils.generateProject();
        entityToSave.setAuthor(savedAuthor);
        ProjectEntity savedEntity = projectRepository.save(entityToSave);
        Integer idForDelete = savedEntity.getId();

        //when
        mockMvc.perform(delete("/projects/{id}", idForDelete))
                .andExpect(status().isOk());

        //then
        Assertions.assertNull(projectRepository.findOneById(idForDelete));
    }

    @Test
    void testDeleteById_WhenIdIsNotFound() throws Exception {
        // given
        int notExitingId = 0;

        //when - //then
        mockMvc.perform(delete("/projects/{id}", notExitingId))
                .andExpect(status().isNotFound());
    }
}

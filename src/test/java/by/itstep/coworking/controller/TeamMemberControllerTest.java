package by.itstep.coworking.controller;

import by.itstep.coworking.dto.teamMember.TeamMemberCreateDto;
import by.itstep.coworking.dto.teamMember.TeamMemberFullDto;
import by.itstep.coworking.entity.TeamMemberEntity;
import by.itstep.coworking.integration.AbstractIntegrationTest;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.List;

import static by.itstep.coworking.EntityGenerationsUtils.generateTeamMember;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TeamMemberControllerTest extends AbstractIntegrationTest {

    @Test
    void testFindAll_HappyPath() throws Exception {
        //given
        List<TeamMemberEntity> memberEntities = new ArrayList<>();
        for(int i = 0; i<10; i++){
            TeamMemberEntity savedMember = memberRepository.save(generateTeamMember());
            memberEntities.add(savedMember);
        }

        Pageable page = PageRequest.of(0,10);

        //when-//then
        mockMvc.perform(get("/teamMembers",page))
                .andExpect(status().isOk());
    }

    @Test
    void testGetById_HappyPath() throws Exception {
        //given
        TeamMemberEntity savedMember = memberRepository.save(generateTeamMember());
        int id = savedMember.getId();

        //when
        MvcResult result = mockMvc.perform(get("/teamMembers/{id}",id))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        TeamMemberFullDto foundDto = objectMapper.readValue(bytes,TeamMemberFullDto.class);

        //then
        Assertions.assertNotNull(foundDto);
        Assertions.assertNotNull(foundDto.getId());
        Assertions.assertEquals(foundDto.getId(), savedMember.getId());
    }

    @Test
    void testGetById_WhenIdIsNotFound() throws Exception {
        //given
        int notExitingId = 0;

        //when - //then
        mockMvc.perform(get("/teamMembers/{id}",notExitingId))
                .andExpect(status().isNotFound());
    }

    @Test
    void testCreate_HappyPath() throws Exception {
        //given
        TeamMemberCreateDto createDto = new TeamMemberCreateDto();
        createDto.setUserId(1);

        //when
        MvcResult result = mockMvc.perform(post("/teamMembers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        TeamMemberFullDto savedTeamMember = objectMapper.readValue(bytes, new TypeReference<TeamMemberFullDto>() {});

        //then
        Assertions.assertNotNull(savedTeamMember);
        Assertions.assertNotNull(savedTeamMember.getId());
    }

    @Test
    void testCreate_WhenUserIdIsNull() throws Exception {
        //given
        TeamMemberCreateDto createDto = new TeamMemberCreateDto();


        //when - //then
        mockMvc.perform(post("/teamMembers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testDeleteById_HappyPath() throws Exception {
        //given
        TeamMemberEntity savedEntity = memberRepository.save(generateTeamMember());
        Integer idForDelete = savedEntity.getId();

        //when
        mockMvc.perform(delete("/teamMembers/{id}", idForDelete))
                .andExpect(status().isOk());

        //then
        Assertions.assertNull(memberRepository.findOneById(idForDelete));
    }

    @Test
    void testDeleteById_WhenIdIsNull() throws Exception {
        //given
        int notExitingId = 0;

        //when - //then
        mockMvc.perform(delete("/teamMembers/{id}", notExitingId))
                .andExpect(status().isNotFound());
    }
}

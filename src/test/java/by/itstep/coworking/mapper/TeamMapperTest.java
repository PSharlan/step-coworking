package by.itstep.coworking.mapper;

import by.itstep.coworking.EntityGenerationsUtils;
import by.itstep.coworking.dto.team.TeamCreateDto;
import by.itstep.coworking.dto.team.TeamFullDto;
import by.itstep.coworking.dto.team.TeamUpdateDto;
import by.itstep.coworking.entity.ProjectEntity;
import by.itstep.coworking.entity.TeamEntity;
import by.itstep.coworking.entity.TeamMemberEntity;
import by.itstep.coworking.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

public class TeamMapperTest extends AbstractIntegrationTest {

    @Test
    void testMapToFullDto_happyPath(){
        //given
        TeamMemberEntity memberEntity = EntityGenerationsUtils.generateTeamMember();
        TeamMemberEntity memberEntity2 = EntityGenerationsUtils.generateTeamMember();
        memberEntity.setId(1);
        memberEntity.setId(2);
        List<TeamMemberEntity> membersList = new ArrayList<>();
        membersList.add(memberEntity);
        membersList.add(memberEntity2);
        ProjectEntity projectEntity = EntityGenerationsUtils.generateProject();
        projectEntity.setId(1);
        TeamEntity teamEntity = EntityGenerationsUtils.generateTeam();
        teamEntity.setId(1);
        teamEntity.setProject(projectEntity);
        teamEntity.setMembers(membersList);

        //when
        TeamFullDto fullDto = TeamMapper.TEAM_MAPPER.toFullDto(teamEntity);

        //then
        Assertions.assertNotNull(fullDto);
        Assertions.assertEquals(fullDto.getId(), teamEntity.getId());
        Assertions.assertEquals(fullDto.getName(), teamEntity.getName());
        Assertions.assertEquals(fullDto.getMembers().size(), teamEntity.getMembers().size());
        Assertions.assertEquals(fullDto.getProject().getId(), teamEntity.getProject().getId());
    }

    @Test
    void testMapFromCreateDtoToEntity_happyPath(){
        //given
        TeamCreateDto createDto = new TeamCreateDto();
        createDto.setName("Team Name");

        //when
        TeamEntity teamEntity = TeamMapper.TEAM_MAPPER.toEntity(createDto);

        //then
        Assertions.assertNotNull(teamEntity);
        Assertions.assertEquals(teamEntity.getName(), createDto.getName());
    }

    @Test
    void testMapFromUpdateDtoToEntity_happyPath(){
        //given
        TeamUpdateDto updateDto = new TeamUpdateDto();
        updateDto.setId(1);
        updateDto.setName("Team Name");

        //when
        TeamEntity teamEntity = TeamMapper.TEAM_MAPPER.toEntity(updateDto);

        //then
        Assertions.assertNotNull(teamEntity);
        Assertions.assertEquals(teamEntity.getId(), updateDto.getId());
        Assertions.assertEquals(teamEntity.getName(), updateDto.getName());
    }
}

package by.itstep.coworking.mapper;

import by.itstep.coworking.EntityGenerationsUtils;
import by.itstep.coworking.dto.teamMember.TeamMemberCreateDto;
import by.itstep.coworking.dto.teamMember.TeamMemberFullDto;
import by.itstep.coworking.entity.JoinRequestEntity;
import by.itstep.coworking.entity.ProjectEntity;
import by.itstep.coworking.entity.TeamMemberEntity;
import by.itstep.coworking.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class TeamMemberTest extends AbstractIntegrationTest {

    @Test
    void testMapToFullDto(){
        //Given
        TeamMemberEntity memberEntity = new TeamMemberEntity();

        JoinRequestEntity joinRequestEntity = EntityGenerationsUtils.generateJoinRequest();
        JoinRequestEntity joinRequestEntity2 = EntityGenerationsUtils.generateJoinRequest();
        joinRequestEntity.setId(1);
        joinRequestEntity2.setId(2);
        joinRequestEntity.setMember(memberEntity);
        joinRequestEntity2.setMember(memberEntity);
        List<JoinRequestEntity> requestEntitiesList = new ArrayList<>();
        requestEntitiesList.add(joinRequestEntity);
        requestEntitiesList.add(joinRequestEntity2);
        memberEntity.setJoinRequests(requestEntitiesList);

        ProjectEntity projectEntity = EntityGenerationsUtils.generateProject();
        ProjectEntity projectEntity2 = EntityGenerationsUtils.generateProject();
        projectEntity.setId(1);
        projectEntity2.setId(2);
        projectEntity.setAuthor(memberEntity);
        projectEntity2.setAuthor(memberEntity);
        List<ProjectEntity>projectEntitiesList = new ArrayList<>();
        projectEntitiesList.add(projectEntity);
        projectEntitiesList.add(projectEntity2);
        memberEntity.setOwnedProjects(projectEntitiesList);

        //when
        TeamMemberFullDto fullDto = TeamMemberMapper.TEAM_MEMBER_MAPPER.toFullDto(memberEntity);

        //then
        Assertions.assertNotNull(fullDto);
        Assertions.assertEquals(fullDto.getId(),memberEntity.getId());
        Assertions.assertEquals(fullDto.getJoinRequests().size(),memberEntity.getJoinRequests().size());
        Assertions.assertEquals(fullDto.getOwnedProjects().size(),memberEntity.getOwnedProjects().size());
    }

    @Test
    void testMapToEntity_HappyPath(){
        //given
        TeamMemberCreateDto createDto = new TeamMemberCreateDto();
        Integer userId = 1;
        createDto.setUserId(userId);

        //when
        TeamMemberEntity memberEntity = TeamMemberMapper.TEAM_MEMBER_MAPPER.toEntity(createDto);

        //then
        Assertions.assertNotNull(memberEntity);
        Assertions.assertEquals(memberEntity.getUserId(), createDto.getUserId());
    }

}

package by.itstep.coworking.mapper;

import by.itstep.coworking.EntityGenerationsUtils;
import by.itstep.coworking.dto.joinRequest.JoinRequestCreateDto;
import by.itstep.coworking.dto.joinRequest.JoinRequestFullDto;
import by.itstep.coworking.entity.JoinRequestEntity;
import by.itstep.coworking.entity.ProjectEntity;
import by.itstep.coworking.entity.TeamMemberEntity;
import by.itstep.coworking.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;


public class JoinRequestMapperTest extends AbstractIntegrationTest {

    @Test
    void mapToFullDto_happyPath(){

        //given
        ProjectEntity project = EntityGenerationsUtils.generateProject();
        project.setId(1);
        TeamMemberEntity teamMember = EntityGenerationsUtils.generateTeamMember();
        teamMember.setId(1);
        JoinRequestEntity entity = EntityGenerationsUtils.generateJoinRequest();
        entity.setId(2);
        entity.setProject(project);
        entity.setMember(teamMember);

        //when
        JoinRequestFullDto fullDto = JoinRequestMapper.JOIN_REQUEST_MAPPER.toFullDto(entity);

        //then
        Assertions.assertNotNull(fullDto);
        Assertions.assertEquals(fullDto.getId(),entity.getId());
        Assertions.assertEquals(fullDto.getMessage(),entity.getMessage());
        Assertions.assertEquals(fullDto.getApproved(),entity.getApproved());
        Assertions.assertEquals(fullDto.getMemberId(),entity.getMember().getId());
        Assertions.assertEquals(fullDto.getProjectId(),entity.getProject().getId());
    }

    @Test
    void mapToEntity_happyPath(){

        //given
        JoinRequestCreateDto createDto = new JoinRequestCreateDto();
        createDto.setUserId(1);
        createDto.setProjectId(1);
        createDto.setMessage("Just do it");

        //when
        JoinRequestEntity entity = JoinRequestMapper.JOIN_REQUEST_MAPPER.toEntity(createDto);

        //then
        Assertions.assertNotNull(entity);
        Assertions.assertEquals(entity.getMessage(), createDto.getMessage());
        Assertions.assertNull(entity.getApproved());
        Assertions.assertNull(entity.getMember());
        Assertions.assertNull(entity.getProject());
    }
}

package by.itstep.coworking.mapper;

import by.itstep.coworking.EntityGenerationsUtils;
import by.itstep.coworking.dto.project.ProjectCreateDto;
import by.itstep.coworking.dto.project.ProjectFullDto;
import by.itstep.coworking.dto.project.ProjectPreviewDto;
import by.itstep.coworking.dto.project.ProjectUpdateDto;
import by.itstep.coworking.entity.JoinRequestEntity;
import by.itstep.coworking.entity.ProjectEntity;
import by.itstep.coworking.entity.TeamEntity;
import by.itstep.coworking.entity.TeamMemberEntity;
import by.itstep.coworking.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;


public class ProjectMapperTest extends AbstractIntegrationTest {

    @Test
    void mapToFullDto_happyPath(){

        //given
        TeamMemberEntity member = EntityGenerationsUtils.generateTeamMember();
        member.setId(1);
        TeamEntity team = EntityGenerationsUtils.generateTeam();
        team.setId(1);
        List<JoinRequestEntity> requestsList = new ArrayList<>();
        JoinRequestEntity request1 = EntityGenerationsUtils.generateJoinRequest();
        JoinRequestEntity request2 = EntityGenerationsUtils.generateJoinRequest();
        JoinRequestEntity request3 = EntityGenerationsUtils.generateJoinRequest();
        request1.setId(1);
        request2.setId(2);
        request3.setId(3);
        requestsList.add(request1);
        requestsList.add(request2);
        requestsList.add(request3);

        ProjectEntity projectEntity = EntityGenerationsUtils.generateProject();
        projectEntity.setId(1);
        projectEntity.setAuthor(member);
        projectEntity.setTeam(team);
        projectEntity.setRequests(requestsList);

        //when
        ProjectFullDto fullDto = ProjectMapper.PROJECT_MAPPER.toFullDto(projectEntity);

        //then
        Assertions.assertNotNull(fullDto);
        Assertions.assertEquals(fullDto.getId(), projectEntity.getId());
        Assertions.assertEquals(fullDto.getAuthorId(), projectEntity.getAuthor().getId());
        Assertions.assertEquals(fullDto.getTeamId(), projectEntity.getTeam().getId());
        Assertions.assertEquals(fullDto.getDateCreated(), projectEntity.getDateCreated());
        Assertions.assertEquals(fullDto.getDescription(), projectEntity.getDescription());
        Assertions.assertEquals(fullDto.getRequests().size(), projectEntity.getRequests().size());
        Assertions.assertEquals(fullDto.getRequests().get(0).getId(), projectEntity.getRequests().get(0).getId());
        Assertions.assertEquals(fullDto.getRequests().get(1).getId(), projectEntity.getRequests().get(1).getId());
        Assertions.assertEquals(fullDto.getRequests().get(2).getId(), projectEntity.getRequests().get(2).getId());
    }

    @Test
    void mapToPreviewDto_happyPath(){

        //given
        TeamMemberEntity member = EntityGenerationsUtils.generateTeamMember();
        member.setId(1);
        ProjectEntity projectEntity = EntityGenerationsUtils.generateProject();
        projectEntity.setId(1);
        projectEntity.setAuthor(member);


        //when
        ProjectPreviewDto previewDto = ProjectMapper.PROJECT_MAPPER.toPreviewDto(projectEntity);

        //then
        Assertions.assertNotNull(previewDto);
        Assertions.assertEquals(previewDto.getId(), projectEntity.getId());
        Assertions.assertEquals(previewDto.getAuthorId(), projectEntity.getAuthor().getId());
        Assertions.assertEquals(previewDto.getDateCreated(), projectEntity.getDateCreated());
        Assertions.assertEquals(previewDto.getDescription(), projectEntity.getDescription());
    }

    @Test
    void mapToEntityFromCreateDto_happyPath(){

        //given
        ProjectCreateDto createDto = new ProjectCreateDto();
        createDto.setAuthorId(1);
        createDto.setDescription("The best of the best project");
        createDto.setName("Bobson project");

        //when
        ProjectEntity projectEntity = ProjectMapper.PROJECT_MAPPER.toEntity(createDto);

        //then
        Assertions.assertNotNull(projectEntity);
        Assertions.assertEquals(projectEntity.getName(), createDto.getName());
        Assertions.assertEquals(projectEntity.getDescription(), createDto.getDescription());
        Assertions.assertEquals(projectEntity.getRequests().size(), 0);
        Assertions.assertNull(projectEntity.getId());
        Assertions.assertNull(projectEntity.getAuthor());
        Assertions.assertNull(projectEntity.getTeam());
    }

    @Test
    void mapToEntityFromUpdateDto_happyPath(){

        //given
        ProjectUpdateDto updateDto = new ProjectUpdateDto();
        updateDto.setId(1);
        updateDto.setDescription("The best of the best project");
        updateDto.setName("Bobson project");

        //when
        ProjectEntity projectEntity = ProjectMapper.PROJECT_MAPPER.toEntity(updateDto);

        //then
        Assertions.assertNotNull(projectEntity);
        Assertions.assertEquals(projectEntity.getId(), updateDto.getId());
        Assertions.assertEquals(projectEntity.getName(), updateDto.getName());
        Assertions.assertEquals(projectEntity.getDescription(), updateDto.getDescription());
        Assertions.assertEquals(projectEntity.getRequests().size(), 0);
        Assertions.assertNull(projectEntity.getAuthor());
        Assertions.assertNull(projectEntity.getTeam());
    }
}

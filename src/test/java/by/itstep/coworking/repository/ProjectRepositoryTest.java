package by.itstep.coworking.repository;

import by.itstep.coworking.EntityGenerationsUtils;
import by.itstep.coworking.entity.ProjectEntity;
import by.itstep.coworking.entity.TeamEntity;
import by.itstep.coworking.entity.TeamMemberEntity;
import by.itstep.coworking.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;


public class ProjectRepositoryTest extends AbstractIntegrationTest {

 @Test
    void save_happyPath(){
        //given
        TeamMemberEntity member = EntityGenerationsUtils.generateTeamMember();
        TeamMemberEntity savedMember = memberRepository.save(member);

        TeamEntity team = EntityGenerationsUtils.generateTeam();
        TeamEntity savedTeam = teamRepository.save(team);


        ProjectEntity project = EntityGenerationsUtils.generateProject();
        project.setAuthor(savedMember);
        project.setTeam(savedTeam);

        //when
        ProjectEntity savedProject = projectRepository.save(project);

        //then
        Assertions.assertNotNull(savedProject);
        Assertions.assertNotNull(savedProject.getId());
        Assertions.assertNotNull(savedProject.getTeam());
        Assertions.assertNotNull(savedProject.getAuthor());
    }

    @Test
    void getById_happyPath(){
        //given
        TeamMemberEntity member = EntityGenerationsUtils.generateTeamMember();
        TeamMemberEntity savedMember = memberRepository.save(member);

        ProjectEntity project = EntityGenerationsUtils.generateProject();
        project.setAuthor(savedMember);
        ProjectEntity savedProject = projectRepository.save(project);

        //when
        ProjectEntity foundProject = projectRepository.findOneById(savedProject.getId());

        //then
        Assertions.assertNotNull(foundProject);
        Assertions.assertEquals(foundProject.getId(), savedProject.getId());
        Assertions.assertEquals(foundProject.getAuthor().getId(), savedProject.getAuthor().getId());
    }

    @Test
    void getAll_happyPath(){
        //given
        List<ProjectEntity> projectsList = new ArrayList<>();

        for (int i = 0; i < 20; i++ ){
            TeamMemberEntity member = EntityGenerationsUtils.generateTeamMember();
            TeamMemberEntity savedMember = memberRepository.save(member);

            ProjectEntity project = EntityGenerationsUtils.generateProject();
            project.setAuthor(savedMember);
            ProjectEntity savedProject = projectRepository.save(project);

            projectsList.add(savedProject);
        }
        //when
        List<ProjectEntity> foundProjects = projectRepository.findAll(Pageable.unpaged()).getContent();

        //then
        Assertions.assertEquals(foundProjects.size(), projectsList.size());
    }

    @Test
    void deleteById_happyPath(){
        //given
        TeamMemberEntity member = EntityGenerationsUtils.generateTeamMember();
        TeamMemberEntity savedMember = memberRepository.save(member);

        ProjectEntity project = EntityGenerationsUtils.generateProject();
        project.setAuthor(savedMember);
        ProjectEntity savedProject = projectRepository.save(project);

        //when
        projectRepository.delete(savedProject);

        //then
        ProjectEntity foundProject = projectRepository.findOneById(savedProject.getId());
        Assertions.assertNull(foundProject);
    }
}

package by.itstep.coworking.repository;

import by.itstep.coworking.EntityGenerationsUtils;
import by.itstep.coworking.entity.TeamEntity;
import by.itstep.coworking.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

public class TeamRepositoryTest extends AbstractIntegrationTest {

    @Test
    void save_happyPath() {
        //given
        TeamEntity team = EntityGenerationsUtils.generateTeam();

        //when
        TeamEntity savedTeam = teamRepository.save(team);

        //then
        Assertions.assertNotNull(savedTeam);
        Assertions.assertNotNull(savedTeam.getId());

    }

    @Test
    void findById_happyPath() {
        //given
        TeamEntity team = EntityGenerationsUtils.generateTeam();
        TeamEntity savedTeam = teamRepository.save(team);

        //when
        TeamEntity foundTeam = teamRepository.findOneById(savedTeam.getId());

        //then
        Assertions.assertNotNull(foundTeam);
        Assertions.assertEquals(foundTeam.getId(), savedTeam.getId());
    }

    @Test
    void findAll_happyPath() {
        //given
        List<TeamEntity> teamsList = new ArrayList<>();
        for (int i = 0; i < 20; i++){
            TeamEntity team = EntityGenerationsUtils.generateTeam();
            TeamEntity savedTeam = teamRepository.save(team);
            teamsList.add(savedTeam);
        }

        //when
        List<TeamEntity> foundTeams = teamRepository.findAll(Pageable.unpaged()).getContent();

        //then
        Assertions.assertEquals(foundTeams.size(), teamsList.size());
    }

    @Test
    void deleteById_happyPath(){
        //given
        TeamEntity team = EntityGenerationsUtils.generateTeam();
        TeamEntity savedTeam = teamRepository.save(team);

        //when
        teamRepository.delete(savedTeam);

        //then
        TeamEntity foundTeam = teamRepository.findOneById(savedTeam.getId());
        Assertions.assertNull(foundTeam);
    }

}

package by.itstep.coworking.repository;

import by.itstep.coworking.EntityGenerationsUtils;
import by.itstep.coworking.entity.TeamMemberEntity;
import by.itstep.coworking.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

public class TeamMemberRepositoryTest extends AbstractIntegrationTest {

   @Test
    void save_happyPath(){
        //given
        TeamMemberEntity member = EntityGenerationsUtils.generateTeamMember();

        //when
        TeamMemberEntity savedMember = memberRepository.save(member);

        //then
        Assertions.assertNotNull(savedMember);
        Assertions.assertNotNull(savedMember.getId());
    }

    @Test
    void findById_happyPath(){
        //given
        TeamMemberEntity member = EntityGenerationsUtils.generateTeamMember();
        TeamMemberEntity savedMember = memberRepository.save(member);

        //when
        TeamMemberEntity foundMember = memberRepository.findOneById(savedMember.getId());

        //then
        Assertions.assertEquals(savedMember.getId(), foundMember.getId());
        Assertions.assertEquals(savedMember.getUserId(), foundMember.getUserId());
    }

    @Test
    void findByUserId_happyPath(){
        //given
        TeamMemberEntity member = EntityGenerationsUtils.generateTeamMember();
        TeamMemberEntity savedMember = memberRepository.save(member);

        //when
        TeamMemberEntity foundMember = memberRepository.findByUserId(savedMember.getUserId());

        //then
        Assertions.assertNotNull(foundMember);
        Assertions.assertEquals(foundMember.getId(),savedMember.getId());
        Assertions.assertEquals(foundMember.getUserId(),savedMember.getUserId());
    }

    @Test
    void findAll_happyPath(){
        //given
        List<TeamMemberEntity> membersList = new ArrayList<>();

        for(int i = 0; i < 20; i ++){
            TeamMemberEntity member = EntityGenerationsUtils.generateTeamMember();
            TeamMemberEntity savedMember = memberRepository.save(member);
            membersList.add(savedMember);
        }
        //when
        List<TeamMemberEntity> foundMembers = memberRepository.findAll(Pageable.unpaged()).getContent();

        //then
        Assertions.assertEquals(foundMembers.size(), membersList.size());
    }

    @Test
    void deleteById(){
        //given
        TeamMemberEntity member = EntityGenerationsUtils.generateTeamMember();
        TeamMemberEntity savedMember = memberRepository.save(member);

        //when
        memberRepository.deleteById(savedMember.getId());

        //then
        TeamMemberEntity foundUser = memberRepository.findOneById(savedMember.getId());
        Assertions.assertNull(foundUser);
    }
}

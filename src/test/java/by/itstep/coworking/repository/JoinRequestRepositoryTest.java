package by.itstep.coworking.repository;

import by.itstep.coworking.EntityGenerationsUtils;
import by.itstep.coworking.entity.JoinRequestEntity;
import by.itstep.coworking.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

public class JoinRequestRepositoryTest extends AbstractIntegrationTest {

    @Test
    void save_happyPath(){
        //given
        JoinRequestEntity joinRequest = EntityGenerationsUtils.generateJoinRequest();

        //when
        JoinRequestEntity savedJoinRequest = requestRepository.save(joinRequest);

        //then
        Assertions.assertNotNull(savedJoinRequest);
        Assertions.assertNotNull(savedJoinRequest.getId());
    }

    @Test
    void getById_happyPath(){
        //given
        JoinRequestEntity joinRequest = EntityGenerationsUtils.generateJoinRequest();
        JoinRequestEntity savedJoinRequest = requestRepository.save(joinRequest);

        //when
        JoinRequestEntity foundJoinRequest = requestRepository.findOneById(savedJoinRequest.getId());

        //then
        Assertions.assertNotNull(foundJoinRequest);
        Assertions.assertEquals(foundJoinRequest.getId(), savedJoinRequest.getId());
    }

    @Test
    void getAll_happyPath(){
        //given
        List<JoinRequestEntity> joinRequestsList = new ArrayList<>();
        for (int i = 0; i < 20; i ++){
            JoinRequestEntity joinRequest = EntityGenerationsUtils.generateJoinRequest();
            JoinRequestEntity savedJoinRequest = requestRepository.save(joinRequest);
            joinRequestsList.add(savedJoinRequest);
        }
        //when
        List<JoinRequestEntity> foundJoinRequests = requestRepository.findAll(Pageable.unpaged()).getContent();

        //then
        Assertions.assertEquals(foundJoinRequests.size(), joinRequestsList.size());
    }

    @Test
    void deleteById_happyPath(){
        //given
        JoinRequestEntity joinRequest = EntityGenerationsUtils.generateJoinRequest();
        JoinRequestEntity savedJoinRequest = requestRepository.save(joinRequest);

        //when
        requestRepository.deleteById(savedJoinRequest.getId());

        //then
        JoinRequestEntity foundJoinRequest = requestRepository.findOneById(savedJoinRequest.getId());
        Assertions.assertNull(foundJoinRequest);
    }
}

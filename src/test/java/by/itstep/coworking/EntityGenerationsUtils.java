package by.itstep.coworking;

import by.itstep.coworking.entity.JoinRequestEntity;
import by.itstep.coworking.entity.ProjectEntity;
import by.itstep.coworking.entity.TeamEntity;
import by.itstep.coworking.entity.TeamMemberEntity;

import java.time.Instant;

public class EntityGenerationsUtils {

    public static JoinRequestEntity generateJoinRequest(){
        JoinRequestEntity joinRequest = new JoinRequestEntity();
        joinRequest.setMessage("Take me with you " + Math.random() * 100);
        joinRequest.setApproved(false);

        return joinRequest;
    }

    public static ProjectEntity generateProject(){
        ProjectEntity project = new ProjectEntity();
        project.setName("Project name " + Math.random() * 100);
        project.setDescription("Description " + Math.random() * 100);
        project.setDateCreated(Instant.now());

        return project;
    }

    public static TeamEntity generateTeam(){
        TeamEntity team = new TeamEntity();
        team.setName("Team name " + Math.random() * 100);

        return team;
    }

    public static TeamMemberEntity generateTeamMember(){
        TeamMemberEntity teamMember = new TeamMemberEntity();
        teamMember.setUserId((int) (Math.random() * 100));

        return teamMember;
    }
}

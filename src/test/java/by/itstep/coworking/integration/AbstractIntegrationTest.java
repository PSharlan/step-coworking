package by.itstep.coworking.integration;

import by.itstep.coworking.StepCoworkingApplication;
import by.itstep.coworking.integration.initalizer.MySqlInitializer;
import by.itstep.coworking.repository.JoinRequestRepository;
import by.itstep.coworking.repository.ProjectRepository;
import by.itstep.coworking.repository.TeamMemberRepository;
import by.itstep.coworking.repository.TeamRepository;
import by.itstep.coworking.service.JoinRequestService;
import by.itstep.coworking.service.ProjectService;
import by.itstep.coworking.service.TeamMemberService;
import by.itstep.coworking.service.TeamService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.junit.jupiter.Testcontainers;

@ContextConfiguration(initializers = MySqlInitializer.class, classes = StepCoworkingApplication.class)
@SpringBootTest
@Testcontainers
@AutoConfigureMockMvc
public abstract class AbstractIntegrationTest {

    @Autowired
    protected JoinRequestRepository requestRepository;

    @Autowired
    protected JoinRequestService requestService;

    @Autowired
    protected ProjectRepository projectRepository;

    @Autowired
    protected ProjectService projectService;

    @Autowired
    protected TeamMemberRepository memberRepository;

    @Autowired
    protected TeamMemberService memberService;

    @Autowired
    protected TeamRepository teamRepository;
    
    @Autowired
    protected TeamService teamService;

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected ObjectMapper objectMapper;

    @BeforeEach
    public void prepareDatabase(){
        teamRepository.deleteAll();
        requestRepository.deleteAll();
        projectRepository.deleteAll();
        memberRepository.deleteAll();
    }
}

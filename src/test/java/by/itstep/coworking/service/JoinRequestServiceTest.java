package by.itstep.coworking.service;

import by.itstep.coworking.EntityGenerationsUtils;
import by.itstep.coworking.dto.joinRequest.JoinRequestCreateDto;
import by.itstep.coworking.dto.joinRequest.JoinRequestFullDto;
import by.itstep.coworking.entity.JoinRequestEntity;
import by.itstep.coworking.entity.ProjectEntity;
import by.itstep.coworking.entity.TeamEntity;
import by.itstep.coworking.entity.TeamMemberEntity;
import by.itstep.coworking.exception.EntityIsNotFoundException;
import by.itstep.coworking.integration.AbstractIntegrationTest;
import by.itstep.coworking.repository.JoinRequestRepository;
import by.itstep.coworking.repository.ProjectRepository;
import by.itstep.coworking.repository.TeamMemberRepository;
import by.itstep.coworking.repository.TeamRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

import static by.itstep.coworking.EntityGenerationsUtils.*;

public class JoinRequestServiceTest extends AbstractIntegrationTest {

    @Test
    void testFindById_happyPath() {
        //given
        JoinRequestEntity requestEntity = generateJoinRequest();
        JoinRequestEntity savedEntity = requestRepository.save(requestEntity);

        //when
        JoinRequestFullDto foundDto = requestService.findById(savedEntity.getId());

        //then
        Assertions.assertNotNull(foundDto);
        Assertions.assertEquals(foundDto.getId(), savedEntity.getId());
    }

    @Test
    void testFindById_whenNotFound(){
        //given
        int notExitingId = 0;

        //when
        Exception exception = Assertions.assertThrows(
                EntityIsNotFoundException.class,() ->requestService.findById(notExitingId));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExitingId)));
    }

    @Test
    void testFindAll_happyPath() {
        //given
        List<JoinRequestEntity> savedEntities = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            JoinRequestEntity requestEntity = generateJoinRequest();
            JoinRequestEntity savedEntity = requestRepository.save(requestEntity);

            savedEntities.add(savedEntity);
        }

        //when
        List<JoinRequestFullDto> foundDtos = requestService.findAll(Pageable.unpaged()).getContent();

        //then
        Assertions.assertNotNull(foundDtos);
        Assertions.assertEquals(foundDtos.size(), savedEntities.size());
    }

    @Test
    void testCreate_happyPath() {
        //given
        JoinRequestCreateDto createDto = new JoinRequestCreateDto();
        TeamMemberEntity savedMember = memberRepository.save(EntityGenerationsUtils.generateTeamMember());
        ProjectEntity project = EntityGenerationsUtils.generateProject();
        project.setAuthor(savedMember);
        ProjectEntity savedProject = projectRepository.save(project);


        createDto.setProjectId(savedProject.getId());
        createDto.setUserId(savedMember.getId());
        createDto.setMessage("Join request message");

        //when
        JoinRequestFullDto foundDto = requestService.create(createDto);
        JoinRequestEntity foundEntity = requestRepository.findOneById(foundDto.getId());

        //then
        Assertions.assertNotNull(foundDto);
        Assertions.assertNotNull(foundEntity);
        Assertions.assertEquals(foundEntity.getId(), foundDto.getId());
        Assertions.assertEquals(foundEntity.getProject().getId(), createDto.getProjectId());
        Assertions.assertEquals(foundEntity.getMember().getUserId(), createDto.getUserId());
        Assertions.assertEquals(foundEntity.getMessage(), createDto.getMessage());
    }

    @Test
    void testCreate_whenProjectNotFound() {
        //given
        JoinRequestCreateDto createDto = new JoinRequestCreateDto();

        int notExistingId = 0;

        createDto.setProjectId(notExistingId);
        createDto.setUserId(2);
        createDto.setMessage("Join request message");

        //when
        Exception exception = Assertions.assertThrows(
                EntityIsNotFoundException.class,()-> requestService.create(createDto));
        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void testCreateWithOutMemberId_happyPath() {
        //given
        JoinRequestCreateDto createDto = new JoinRequestCreateDto();
        TeamMemberEntity savedMember = memberRepository.save(EntityGenerationsUtils.generateTeamMember());
        ProjectEntity project = EntityGenerationsUtils.generateProject();
        project.setAuthor(savedMember);
        ProjectEntity savedProject = projectRepository.save(project);

        TeamMemberEntity newMember = EntityGenerationsUtils.generateTeamMember();

        createDto.setProjectId(savedProject.getId());
        createDto.setUserId(newMember.getUserId());
        createDto.setMessage("Join request message");

        //when
        JoinRequestFullDto foundDto = requestService.create(createDto);
        JoinRequestEntity foundEntity = requestRepository.findOneById(foundDto.getId());

        //then
        Assertions.assertNotNull(foundDto);
        Assertions.assertNotNull(foundEntity);
        Assertions.assertNotNull(foundEntity.getMember().getId());
        Assertions.assertEquals(foundEntity.getId(), foundDto.getId());
        Assertions.assertEquals(foundEntity.getProject().getId(), createDto.getProjectId());
        Assertions.assertEquals(foundEntity.getMember().getUserId(), createDto.getUserId());
        Assertions.assertEquals(foundEntity.getMessage(), createDto.getMessage());
    }

    @Test
    void testDeleteById_happyPath() {
        //given
        JoinRequestEntity entityToSave = generateJoinRequest();
        JoinRequestEntity savedEntity = requestRepository.save(entityToSave);
        Integer idForDelete = savedEntity.getId();

        //when
        requestService.deleteById(idForDelete);

        //then
        Assertions.assertNull(requestRepository.findOneById(idForDelete));
    }

    @Test
    void testDeleteById_whenNotFound() {
        //given
        int notExitingId = 0;

        //when
        Exception exception = Assertions.assertThrows(EntityIsNotFoundException.class,
                ()-> requestService.findById(notExitingId));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExitingId)));
    }

    @Test
    void testAccept_happyPath(){
        //given
        JoinRequestEntity joinRequest = generateJoinRequest();
        ProjectEntity project = generateProject();
        TeamMemberEntity author = generateTeamMember();
        TeamEntity team = generateTeam();

        TeamMemberEntity savedAuthor = memberRepository.save(author);

        project.setAuthor(savedAuthor);
        ProjectEntity savedProject = projectRepository.save(project);

        List<TeamMemberEntity> teamMembers = team.getMembers();
        teamMembers.add(savedAuthor);
        team.setMembers(teamMembers);
        team.setProject(savedProject);
        TeamEntity savedTeam = teamRepository.save(team);

        TeamMemberEntity newTeamMember = memberRepository.save(generateTeamMember());
        joinRequest.setProject(savedProject);
        joinRequest.setMember(newTeamMember);
        JoinRequestEntity savedJoinRequest = requestRepository.save(joinRequest);

        //when
        requestService.accept(savedJoinRequest.getId());

        //then
        JoinRequestEntity acceptedJoinRequest = requestRepository.findOneById(savedJoinRequest.getId());
        TeamEntity updatedTeam = teamRepository.findOneById(savedTeam.getId());
        Assertions.assertEquals(acceptedJoinRequest.getApproved(),true);
        Assertions.assertTrue(updatedTeam.getMembers().contains(newTeamMember));
    }

    @Test
    void testAccept_whenJoinRequestIsNotFound(){
        //given
        int notExitingId = 0;

        //when
        Exception exception = Assertions.assertThrows
                (EntityIsNotFoundException.class,()-> requestService.accept(notExitingId));

        //then
       Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExitingId)));
    }
}

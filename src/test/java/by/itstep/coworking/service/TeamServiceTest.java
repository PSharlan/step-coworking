package by.itstep.coworking.service;

import by.itstep.coworking.dto.team.TeamCreateDto;
import by.itstep.coworking.dto.team.TeamFullDto;
import by.itstep.coworking.dto.team.TeamUpdateDto;
import by.itstep.coworking.entity.ProjectEntity;
import by.itstep.coworking.entity.TeamEntity;
import by.itstep.coworking.entity.TeamMemberEntity;
import by.itstep.coworking.exception.EntityIsNotFoundException;
import by.itstep.coworking.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

import static by.itstep.coworking.EntityGenerationsUtils.*;

public class TeamServiceTest extends AbstractIntegrationTest {

    @Test
    void testFindById_HappyPath() {
        //given
        TeamMemberEntity savedMember = memberRepository.save(generateTeamMember());
        ProjectEntity projectToSave = generateProject();
        projectToSave.setAuthor(savedMember);
        ProjectEntity savedProject = projectRepository.save(projectToSave);

        List<TeamMemberEntity> membersList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            TeamMemberEntity savedMemberEntity = memberRepository.save(generateTeamMember());
            membersList.add(savedMemberEntity);
        }

        TeamEntity teamToSave = generateTeam();
        teamToSave.setProject(savedProject);
        teamToSave.setMembers(membersList);
        TeamEntity savedTeam = teamRepository.save(teamToSave);

        //when
        TeamFullDto foundDto = teamService.findById(savedTeam.getId());

        //then
        Assertions.assertNotNull(foundDto);
        Assertions.assertEquals(foundDto.getName(), savedTeam.getName());
        Assertions.assertEquals(foundDto.getProject().getId(), savedTeam.getProject().getId());
        Assertions.assertEquals(foundDto.getMembers().size(), savedTeam.getMembers().size());
    }

    @Test
    void testFindById_whenNotFound() {
        //given
        int notExitingId = 0;

        //when
        Exception exception = Assertions.assertThrows(EntityIsNotFoundException.class,
                () -> teamService.findById(notExitingId));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExitingId)));
    }

    @Test
    void testFindAll_happyPath() {
        //given
        List<TeamEntity> teamEntities = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            TeamMemberEntity savedMember = memberRepository.save(generateTeamMember());
            ProjectEntity projectToSave = generateProject();
            projectToSave.setAuthor(savedMember);
            ProjectEntity savedProject = projectRepository.save(projectToSave);

            List<TeamMemberEntity> membersList = new ArrayList<>();
            for (int a = 0; a < 10; a++) {
                TeamMemberEntity savedMemberEntity = memberRepository.save(generateTeamMember());
                membersList.add(savedMemberEntity);
            }

            TeamEntity teamToSave = generateTeam();
            teamToSave.setProject(savedProject);
            teamToSave.setMembers(membersList);
            TeamEntity savedTeam = teamRepository.save(teamToSave);
            teamEntities.add(savedTeam);
        }

        //when
        List<TeamFullDto> foundDtos = teamService.findAll(Pageable.unpaged()).getContent();

        //then
        Assertions.assertNotNull(foundDtos);
        Assertions.assertEquals(foundDtos.size(), teamEntities.size());
    }

    @Test
    void testCreate_happyPath() {
        //given
        TeamMemberEntity savedMember = memberRepository.save(generateTeamMember());
        ProjectEntity projectToSave = generateProject();
        projectToSave.setAuthor(savedMember);
        ProjectEntity savedProject = projectRepository.save(projectToSave);

        TeamCreateDto createDto = new TeamCreateDto();
        createDto.setName("Name");
        createDto.setProjectId(savedProject.getId());

        //when
        TeamFullDto foundDto = teamService.create(createDto);

        //then
        Assertions.assertNotNull(foundDto);
        Assertions.assertEquals(foundDto.getName(), createDto.getName());
        Assertions.assertEquals(foundDto.getProject().getId(), createDto.getProjectId());
    }

    @Test
    void testUpdate_happyPath() {
        //given
        TeamMemberEntity savedMember = memberRepository.save(generateTeamMember());
        ProjectEntity projectToSave = generateProject();
        projectToSave.setAuthor(savedMember);
        ProjectEntity savedProject = projectRepository.save(projectToSave);

        List<TeamMemberEntity> membersList = new ArrayList<>();
        for (int a = 0; a < 10; a++) {
            TeamMemberEntity savedMemberEntity = memberRepository.save(generateTeamMember());
            membersList.add(savedMemberEntity);
        }

        TeamEntity teamToSave = generateTeam();
        teamToSave.setProject(savedProject);
        teamToSave.setMembers(membersList);
        TeamEntity savedTeam = teamRepository.save(teamToSave);

        TeamUpdateDto updateDto = new TeamUpdateDto();
        updateDto.setId(savedTeam.getId());
        updateDto.setName("New Name");

        //when
        TeamFullDto foundDto = teamService.update(updateDto);

        //then
        Assertions.assertNotNull(foundDto);
        Assertions.assertEquals(foundDto.getId(), updateDto.getId());
        Assertions.assertEquals(foundDto.getName(), updateDto.getName());

        Assertions.assertEquals(foundDto.getProject().getId(), savedTeam.getProject().getId());
        Assertions.assertEquals(foundDto.getMembers().size(), savedTeam.getMembers().size());
    }

    @Test
    void testUpdate_whenNotFound() {
        //given
        int notExitingId = 0;
        TeamUpdateDto updateDto = new TeamUpdateDto();
        updateDto.setId(notExitingId);
        updateDto.setName("New Name");

        //when
        Exception exception = Assertions.assertThrows(EntityIsNotFoundException.class,
                () -> teamService.update(updateDto));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExitingId)));
    }

    @Test
    void testDeleteById_happyPath() {
        //given
        TeamMemberEntity savedMember = memberRepository.save(generateTeamMember());
        ProjectEntity projectToSave = generateProject();
        projectToSave.setAuthor(savedMember);
        ProjectEntity savedProject = projectRepository.save(projectToSave);

        List<TeamMemberEntity> membersList = new ArrayList<>();
        for (int a = 0; a < 10; a++) {
            TeamMemberEntity savedMemberEntity = memberRepository.save(generateTeamMember());
            membersList.add(savedMemberEntity);
        }

        TeamEntity teamToSave = generateTeam();
        teamToSave.setProject(savedProject);
        teamToSave.setMembers(membersList);
        TeamEntity savedTeam = teamRepository.save(teamToSave);
        Integer idForDelete = savedTeam.getId();

        //when
        teamService.deleteById(idForDelete);

        //then
        Assertions.assertNull(teamRepository.findOneById(idForDelete));
    }

    @Test
    void testDeleteById_whenNotFound() {
        //given
        int notExitingId = 0;

        //when
        Exception exception = Assertions.assertThrows(EntityIsNotFoundException.class,
                () -> teamService.deleteById(notExitingId));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExitingId)));
    }

    @Test
    void testAddMemberToTeam_happyPath() {
        //given
        TeamMemberEntity author = memberRepository.save(generateTeamMember());
        ProjectEntity projectToSave = generateProject();
        projectToSave.setAuthor(author);
        ProjectEntity savedProject = projectRepository.save(projectToSave);

        List<TeamMemberEntity> membersList = new ArrayList<>();
        for (int a = 0; a < 10; a++) {
            TeamMemberEntity savedMemberEntity = memberRepository.save(generateTeamMember());
            membersList.add(savedMemberEntity);
        }

        TeamEntity teamToSave = generateTeam();
        teamToSave.setProject(savedProject);
        teamToSave.setMembers(membersList);
        TeamEntity savedTeam = teamRepository.save(teamToSave);
        Integer teamId = savedTeam.getId();

        TeamMemberEntity savedNewMember = memberRepository.save(generateTeamMember());
        Integer newMemberId = savedNewMember.getId();

        //when
        teamService.addTeamMemberToTeam(teamId, newMemberId);

        //then
        TeamEntity updatedTeam = teamRepository.findOneById(teamId);
        Assertions.assertTrue(updatedTeam.getMembers().contains(savedNewMember));
    }

    @Test
    void testAddMemberToTeam_whenTeamMemberIsNotFound() {
        //given
        TeamMemberEntity author = memberRepository.save(generateTeamMember());
        ProjectEntity projectToSave = generateProject();
        projectToSave.setAuthor(author);
        ProjectEntity savedProject = projectRepository.save(projectToSave);

        List<TeamMemberEntity> membersList = new ArrayList<>();
        for (int a = 0; a < 10; a++) {
            TeamMemberEntity savedMemberEntity = memberRepository.save(generateTeamMember());
            membersList.add(savedMemberEntity);
        }

        TeamEntity teamToSave = generateTeam();
        teamToSave.setProject(savedProject);
        teamToSave.setMembers(membersList);
        TeamEntity savedTeam = teamRepository.save(teamToSave);
        Integer teamId = savedTeam.getId();

        int notExitingMemberId = 0;

        //when
        Exception exception = Assertions.assertThrows(EntityIsNotFoundException.class,
                () -> teamService.addTeamMemberToTeam(teamId, notExitingMemberId));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExitingMemberId)));
    }

    @Test
    void testAddMemberToTeam_whenTeamIsNotFound() {
        //given
        TeamMemberEntity savedMember = memberRepository.save(generateTeamMember());
        int memberId = savedMember.getId();
        int notExitingTeamId = 0;

        //when
        Exception exception = Assertions.assertThrows(EntityIsNotFoundException.class,
                () -> teamService.addTeamMemberToTeam(notExitingTeamId, memberId));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExitingTeamId)));
    }
}

package by.itstep.coworking.service;

import by.itstep.coworking.EntityGenerationsUtils;
import by.itstep.coworking.dto.project.ProjectCreateDto;
import by.itstep.coworking.dto.project.ProjectFullDto;
import by.itstep.coworking.dto.project.ProjectPreviewDto;
import by.itstep.coworking.dto.project.ProjectUpdateDto;
import by.itstep.coworking.entity.JoinRequestEntity;
import by.itstep.coworking.entity.ProjectEntity;
import by.itstep.coworking.entity.TeamEntity;
import by.itstep.coworking.entity.TeamMemberEntity;
import by.itstep.coworking.exception.EntityIsNotFoundException;
import by.itstep.coworking.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProjectServiceTest extends AbstractIntegrationTest {

    @Test
    void testFindById_happyPath(){
        //given
        ProjectEntity entityToSave = EntityGenerationsUtils.generateProject();
        TeamMemberEntity authorToSave = EntityGenerationsUtils.generateTeamMember();
        TeamMemberEntity savedAuthor = memberRepository.save(authorToSave);
        entityToSave.setAuthor(savedAuthor);
        ProjectEntity savedProject = projectRepository.save(entityToSave);

        //when
        ProjectFullDto foundDto = projectService.findById(savedProject.getId());

        //then
        Assertions.assertNotNull(foundDto);
        Assertions.assertEquals(foundDto.getId(),savedProject.getId());
        Assertions.assertEquals(foundDto.getDateCreated().getEpochSecond(),
                savedProject.getDateCreated().getEpochSecond());
        Assertions.assertEquals(foundDto.getAuthorId(),savedProject.getAuthor().getId());
    }

    @Test
    void testFindById_whenNotFound(){
        //given
        int notExitingId = 0;
        //when
        Exception exception = Assertions.assertThrows(
                EntityIsNotFoundException.class,() -> projectService.findById(notExitingId));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExitingId)));
    }

    @Test
    void testFindAll_happyPath(){
        //given
        List<ProjectEntity> projectEntities = new ArrayList<>();
        for (int i = 0; i<20; i++){
            ProjectEntity entityToSave = EntityGenerationsUtils.generateProject();
            TeamMemberEntity authorToSave = EntityGenerationsUtils.generateTeamMember();
            TeamMemberEntity savedAuthor = memberRepository.save(authorToSave);
            entityToSave.setAuthor(savedAuthor);
            ProjectEntity savedProject = projectRepository.save(entityToSave);
            projectEntities.add(savedProject);
        }

        //when
        List<ProjectPreviewDto> foundDto = projectService.findAll(Pageable.unpaged()).getContent();

        //then
        Assertions.assertNotNull(foundDto);
        Assertions.assertEquals(foundDto.size(),projectEntities.size());
    }

    @Test
    void testCreate_happyPath(){
        //given
        ProjectCreateDto createDto = new ProjectCreateDto();
        TeamMemberEntity memberToSave = EntityGenerationsUtils.generateTeamMember();
        TeamMemberEntity savedMember = memberRepository.save(memberToSave);
        createDto.setAuthorId(savedMember.getId());
        createDto.setUserId(savedMember.getUserId());
        createDto.setName("Project name");
        createDto.setDescription("Project description");

        //when
        ProjectFullDto foundDto = projectService.create(createDto);

        //then
        Assertions.assertNotNull(foundDto);
        Assertions.assertNotNull(foundDto.getId());
        Assertions.assertNotNull(foundDto.getDateCreated());
        Assertions.assertEquals(foundDto.getAuthorId(),createDto.getAuthorId());
        Assertions.assertEquals(foundDto.getDescription(),createDto.getDescription());
        Assertions.assertEquals(foundDto.getName(),createDto.getName());

        TeamEntity teamForProject = teamRepository.findOneById(foundDto.getTeamId());
        Assertions.assertNotNull(teamForProject);
        Assertions.assertTrue(teamForProject.getMembers().contains(savedMember));
    }

    @Test
    void testCreateWithOutAuthor_happyPath(){
        //given
        ProjectCreateDto createDto = new ProjectCreateDto();
        TeamMemberEntity memberToSave = EntityGenerationsUtils.generateTeamMember();
        createDto.setUserId(memberToSave.getUserId());
        createDto.setName("Project name");
        createDto.setDescription("Project description");

        //when
        ProjectFullDto foundDto = projectService.create(createDto);

        //then
        TeamMemberEntity savedMember = memberRepository.findByUserId(memberToSave.getUserId());

        Assertions.assertNotNull(foundDto);
        Assertions.assertNotNull(foundDto.getId());
        Assertions.assertNotNull(foundDto.getDateCreated());
        Assertions.assertEquals(foundDto.getAuthorId(),savedMember.getId());
        Assertions.assertEquals(foundDto.getDescription(),createDto.getDescription());
        Assertions.assertEquals(foundDto.getName(),createDto.getName());

        TeamEntity teamForProject = teamRepository.findOneById(foundDto.getTeamId());
        Assertions.assertNotNull(teamForProject);
        Assertions.assertTrue(teamForProject.getMembers().contains(savedMember));
    }

    @Test
    void testUpdate_happyPath(){
        //given

        TeamMemberEntity authorToSave = EntityGenerationsUtils.generateTeamMember();
        TeamMemberEntity savedAuthor = memberRepository.save(authorToSave);

        List<JoinRequestEntity> requests = Arrays.asList(EntityGenerationsUtils.generateJoinRequest());

        ProjectEntity projectToSave = EntityGenerationsUtils.generateProject();
        projectToSave.setAuthor(savedAuthor);
        projectToSave.setRequests(requests);

        ProjectEntity savedProject = projectRepository.save(projectToSave);

        TeamEntity teamToSave = EntityGenerationsUtils.generateTeam();
        teamToSave.setProject(savedProject);
        teamRepository.save(teamToSave);

        ProjectUpdateDto updateDto = new ProjectUpdateDto();
        updateDto.setId(savedProject.getId());
        updateDto.setDescription("New description");
        updateDto.setName("New name");

        ProjectEntity entityToUpdate = projectRepository.findOneById(savedProject.getId());

        //when
        ProjectFullDto foundDto = projectService.update(updateDto);

        //then
        Assertions.assertNotNull(foundDto);
        Assertions.assertEquals(foundDto.getId(), updateDto.getId());
        Assertions.assertEquals(foundDto.getDescription(), updateDto.getDescription());
        Assertions.assertEquals(foundDto.getName(), updateDto.getName());

        Assertions.assertEquals(foundDto.getAuthorId(),entityToUpdate.getAuthor().getId());
        Assertions.assertEquals(foundDto.getDateCreated().getEpochSecond(),
                entityToUpdate.getDateCreated().getEpochSecond());
        Assertions.assertEquals(foundDto.getTeamId(),entityToUpdate.getTeam().getId());
        Assertions.assertEquals(foundDto.getRequests().size(),entityToUpdate.getRequests().size());
    }

    @Test
    void testUpdate_whenProjectNotFound(){
        //given
        int notExitingId = 0;

        ProjectUpdateDto updateDto = new ProjectUpdateDto();
        updateDto.setId(notExitingId);
        updateDto.setDescription("New description");
        updateDto.setName("New name");



        //when
        Exception exception = Assertions.assertThrows(
                EntityIsNotFoundException.class,()-> projectService.update(updateDto));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExitingId)));
    }

    @Test
    void testDeleteById_happyPath(){

        //given
        TeamMemberEntity authorToSave = EntityGenerationsUtils.generateTeamMember();
        TeamMemberEntity savedAuthor = memberRepository.save(authorToSave);

        ProjectEntity entityToSave = EntityGenerationsUtils.generateProject();
        entityToSave.setAuthor(savedAuthor);
        ProjectEntity savedEntity = projectRepository.save(entityToSave);
        Integer idForDelete = savedEntity.getId();

        //when
        projectService.deleteById(idForDelete);

        //then
        Assertions.assertNull(projectRepository.findOneById(idForDelete));
    }

    @Test
    void testDeleteById_whenNotFound(){

        //given
        int notExitingId = 0;

        //when
        Exception exception = Assertions.assertThrows(EntityIsNotFoundException.class,
                ()-> projectService.findById(notExitingId));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExitingId)));
    }
}

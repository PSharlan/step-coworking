package by.itstep.coworking.service;

import by.itstep.coworking.dto.teamMember.TeamMemberCreateDto;
import by.itstep.coworking.dto.teamMember.TeamMemberFullDto;
import by.itstep.coworking.entity.TeamMemberEntity;
import by.itstep.coworking.exception.EntityIsNotFoundException;
import by.itstep.coworking.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

import static by.itstep.coworking.EntityGenerationsUtils.generateTeamMember;

public class TeamMemberServiceTest extends AbstractIntegrationTest {

    @Test
    void testFindById_happyPath(){
        //given
        TeamMemberEntity savedMember = memberRepository.save(generateTeamMember());

        //when
        TeamMemberFullDto foundDto = memberService.findById(savedMember.getId());

        //then
        Assertions.assertNotNull(foundDto);
        Assertions.assertEquals(foundDto.getId(),savedMember.getId());
        Assertions.assertEquals(foundDto.getUserId(),savedMember.getUserId());
    }

    @Test
    void testFindById_whenNotFound(){
        //given
        int notExiting = 0;

        //when
        Exception exception = Assertions.assertThrows(
                EntityIsNotFoundException.class, ()-> memberService.findById(notExiting));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExiting)));
    }

    @Test
    void testFindByUserId_happyPath(){
        //given
        TeamMemberEntity savedMember = memberRepository.save(generateTeamMember());

        //when
        TeamMemberFullDto foundDto = memberService.findByUserId(savedMember.getUserId());

        //Then
        Assertions.assertNotNull(foundDto);
        Assertions.assertEquals(foundDto.getId(), savedMember.getId());
        Assertions.assertEquals(foundDto.getUserId(), savedMember.getUserId());
    }

    @Test
    void testFindByUserId_whenNotFound(){
        //given
        int notExitingId = 0;

        //when
        TeamMemberFullDto foundDto = memberService.findByUserId(notExitingId);

        //Then
        Assertions.assertNull(foundDto);
    }

    @Test
    void testFindAll_HappyPath(){
        //given
        List<TeamMemberEntity> memberEntities = new ArrayList<>();
        for(int i = 0; i<10; i++){
            TeamMemberEntity savedMember = memberRepository.save(generateTeamMember());
            memberEntities.add(savedMember);
        }

        //when
        List<TeamMemberFullDto> foundDtos = memberService.findAll(Pageable.unpaged()).getContent();

        //then
        Assertions.assertNotNull(foundDtos);
        Assertions.assertEquals(foundDtos.size(), memberEntities.size());
    }

    @Test
    void testCreate_happyPath(){
        //given
        TeamMemberCreateDto createDto = new TeamMemberCreateDto();
        createDto.setUserId(1);

        //when
        TeamMemberFullDto foundDto = memberService.create(createDto);

        //then
        Assertions.assertNotNull(foundDto);
        Assertions.assertNotNull(foundDto.getId());
        Assertions.assertEquals(foundDto.getUserId(),createDto.getUserId());
    }

    @Test
    void testDeleteById_happyPath(){
        //given
        TeamMemberEntity savedEntity = memberRepository.save(generateTeamMember());
        Integer idForDelete = savedEntity.getId();

        //when
        memberService.deleteById(idForDelete);

        //then
        Assertions.assertNull(memberRepository.findOneById(idForDelete));
    }

    @Test
    void testDeleteById_whenNotFound(){
        //given
        int notExitingId = 0;

        //when
        Exception exception = Assertions.assertThrows(EntityIsNotFoundException.class,
                ()-> memberService.deleteById(notExitingId));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExitingId)));
    }
}

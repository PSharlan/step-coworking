FROM openjdk:11

ADD target/*.jar /app_step-coworking.jar

ENTRYPOINT ["java", "-jar", "app_step-coworking.jar"]
